# Swoole快速上手手册

[toc]

## 0 扫盲篇

### 0.1 Linux相关基础

#### 0.1.1 centos安装

##### 0.1.1.1 安装

​		请参考这篇文章：[Linux就该这么学——安装配置VM虚拟机 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/11921659.html)

##### 0.1.1.2 配置对外访问网络

​		请参考这篇文章：[vm虚拟机配置对外访问网络 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/13113535.html)

##### 0.1.1.3 更新镜像源

​		请参考这篇文章：[centos镜像_centos下载地址_centos安装教程-阿里巴巴开源镜像站 (aliyun.com)](https://developer.aliyun.com/mirror/centos?spm=a2c6h.13651102.0.0.3e221b11JLUEIf)

##### 0.1.1.4 宝塔面板安装

请参考这篇文章：[宝塔面板下载，免费全能的服务器运维软件 (bt.cn)](https://www.bt.cn/new/download.html)

![image-20230101234116461](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230101234116461.png)

#### 0.1.2 Linux文件结构介绍

​		请参考这篇文章：[Linux就该这么学——一切从”/”开始 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12248761.html)

#### 0.1.3 Linux相关基础命令

​		请参考这些文章

* [Linux就该这么学——新手必须掌握的命令之我的第一个命令 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12037735.html)
* [Linux就该这么学——新手必须掌握的命令之常用的系统工作命令 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12041470.html)
* [Linux就该这么学——新手必须掌握的命令之系统状态检测命令组 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12041501.html)
* [Linux就该这么学——新手必须掌握的命令之工作目录切换命令组 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12041512.html)
* [Linux就该这么学——新手必须掌握的命令之文件编辑命令组 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12041547.html)
* [Linux就该这么学——新手必须掌握的命令之文件目录管理命令组 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12041563.html)
* [Linux就该这么学——新手必须掌握的命令之打包压缩与搜索命令组 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12041587.html)
* [Linux就该这么学——初识重定向 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12046380.html)
* [Linux就该这么学——初识管道符 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12046401.html)

#### 0.1.4 shell快速入门

​		请参考这些文章

* [Linux就该这么学——初识vim编辑器 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12079697.html)
* [Linux就该这么学——编写SHELL脚本 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12182584.html)
* [Linux就该这么学——流程控制语句 - 努力跟上大神的脚步 - 博客园 (cnblogs.com)](https://www.cnblogs.com/studyandstudy/p/12194733.html)

#### 0.1.5 Linux实战练习之源码编译lnp环境

​		请参考这篇文章：[如何在linux下手动构建PHP项目运行环境](https://blog.lt996.cn/front/article?id=6)

#### 0.1.6 相关面试指导

![image-20230101235323906](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230101235323906.png)

### 0.2 swoole初体验

#### 0.2.1 swoole介绍

​		swoole官网：https://www.swoole.com/ 

​		swoole文档：https://wiki.swoole.com/ 

​		开源中国源码：https://gitee.com/swoole/swoole/tree/master/ 

​		根据swoole的介绍：Swoole是一个为PHP用C和C++编写的基于事件的高性能异步& 协程并行网络通信引擎 

​		使 PHP 开发人员可以编写高性能的协程 TCP、UDP、Unix Socket、HTTP，WebSocket 服务。Swoole 可以广泛应用于互联网、移动通信、企业软件、云计算、网络游戏、物联网（IOT）、车联网、智能家居等领域。

 		使 用 PHP + Swoole 作为网络通信框架，可以使企业 IT 研发团队的效率大大提升，更加专注于开发创新产品。

​		拆分理解： 

1. swoole是一个php的扩展， 是由C/C++编辑的
2. swoole是基于事件的 高性能异步&协程并行 的网络通信引擎

##### 0.2.1.1 swoole具备什么功能？

​		swoole提供了哪些功能给我们用，以为我们用到哪些服务时，可以用swoole来帮我们实现。 

* http服务 ，编写一个简单的web server。 

* TCP/UDP服务 ，编写一个消息接受处理系统。 

* 异步，可以异步的处理请求。 

* 并发 ，可以并发的处理同一个业务逻辑。

* socket，socket通讯处理技术。 

* 毫秒级别定时器，可以在php中使用定时器了。 

* 协程，相比线程更稳定和好用。

##### 0.2.1.2 基于swoole框架

* **Swoft** : 首个基于 Swoole 原生协程的新时代 PHP 高性能协程全栈框架，内置协程网络服务器及常用的协程客户端，常驻内存，不依赖传统的 PHP-FPM，全异步非阻塞 IO 实现，以类似于同步客户端的写法实现异步客户端的 使用，没有复杂的异步回调，没有繁琐的 yield, 有类似 Go 语言的协程、灵活的注解、强大的全局依赖注入容器、完善的服务治理、灵活强大的 AOP、标准的 PSR 规范实现等等，可以用于构建高性能的Web系统、 API、中间件、基础服务等等。
* **EasySwoole** : 是一款基于Swoole Server 开发的常驻内存型PHP框架，专为API而生，摆脱传统PHP运行模式在进程唤起和文件加载上带来的性能损失。EasySwoole 高度封装了Swoole Server 而依旧维持Swoole Server 原有特性，支持 同时混合监听HTTP、自定义TCP、UDP协议，让开发者以最低的学习成本和精力编写出多进程，可异步，高可用的应用服务。
* **SwooleDistributed** : 老牌Swoole框架拥有最完善的开发工具以及最强大的功能，首创SDHelper开发者工具包和开发者调试命令集，可以进行单元测试，捕获客户端流量分析，可视化的进行远程断点联调，还具备代码覆盖率检测的功能 （swoole与xdebug扩展不兼容，SDHelper无需xdebug扩展），并且内置组件极其丰富（类MQTT强悍的订阅发布/Actor模型/内存高速缓存/事件派发/进程管理/定时任务/AMQP任务调度/后台监控/集群/微服务/RPC/异 步连接池/自定义命令等等），开发者可以直接使用加快开发进度。几乎所有的功能都支持集群化，单机切换到集群无需对代码做任何的修改。如果业务开发比较复杂比如（游戏开发）那么SD框架将是你的不二之选

##### 0.2.1.3 swoole的应用

​		如果你的业务中，有用到以上等特性，你又在用使用php，那么完全可以用swoole来完成了,再具体点的场景如下：

```tex
1. 互联网
2. 移动通信
3. 企业软件
4. 云计算
5. 网络游戏
6. 物联网（IOT）
7. 车联网
8. 智能家居等领域
```

​		可以配合与传统型框架比如laravel，thinkphp6.0进行加速（注意这些框架是ioc容器为核心的）

​		本质就在于c/s 应用

#### 0.2.2 swoole安装

##### 0.2.2.1 安装swoole前提

​		实验环境：vm虚拟机+centos7

* 安装方式1：通过源码安装lnmp运行环境(文章参考：[如何在linux下手动构建PHP项目运行环境](https://blog.lt996.cn/front/article?id=6))

* 安装方式2：通过宝塔面板一键安装环境（本次使用该实验环境）

##### 0.2.2.2 swoole安装

​		首先去GitHub下载一份swoole的源码随意放置位置 https://github.com/swoole/swoole-src/releases ; 注意!!下载的版本是.

```tex
[root@localhost /]# cd /root
[root@localhost root]# tar -xvf swoole-src-4.4.12.tar.gz
[root@localhost root]# cd swoole-src-4.4.12
[root@localhost swoole-src-4.4.12]# phpize
[root@localhost swoole-src-4.4.12]# ./configure
[root@localhost swoole-src-4.4.12]# make
[root@localhost swoole-src-4.4.12]# sudo make install
```

​		注意!! 在执行 ./configure 的时候容易会出现如下错误

```tex
configure: error: Cannot find PHP-config. Please use --with-php-config=PATH
```

​		问题的意思就是因为PHP的配置文件没有找到,需要额外的指定如下

```tex
[root@localhost swoole-src-4.4.12]# ./configure --with-php-config=/usr/local/php/bin/php-config
```

​		你可以通过下面的命令既可以查找到php-config的文件地址

```tex
[root@localhost swoole-src-4.4.12]# find / -name php-config
/www/server/php/73/src/scripts/php-config
/www/server/php/73/bin/php-config
```

​		所以我的安装执行是

```tex
[root@localhost swoole-src-4.4.12]# ./configure --with-php-config=/www/server/php/73/bin/php-config
[root@localhost swoole-src-4.4.12]# make
[root@localhost swoole-src-4.4.12]# sudo make install
```

![image-20230105120841531](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230105120841531.png)

​		如下便是执行安装,并且安装到了PHP的环境目录中了;最后执行配置好php.ini在里面添加swoole的扩展即可

```tex
[root@localhost swoole-src-4.4.12]# find / -name php.ini
/www/server/php/73/etc/php.ini
[root@localhost swoole-src-4.4.12]# vi /www/server/php/73/etc/php.ini
```

​		添加

```tex
extension=swoole.so
```

​		然后通过php -m检测，最后就是重启即可

```tex
[root@localhost swoole-src-4.4.12]# /etc/init.d/php-fpm-73 restart
Reload service php-fpm done
```

#### 0.2.3 Linux其他安装之composer安装

​		直接根据composer的官网提供方式去操作就OK了

```tex
[root@localhost ~]# php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
[root@localhost ~]# php -r "if (hash_file('sha384', 'composer-setup.php') ===
'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else {
echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
Installer verified
[root@localhost ~]# php composer-setup.php
All settings correct for using Composer
Downloading...
Composer (version 1.9.0) successfully installed to: /root/composer.phar
Use it: php composer.phar
[root@localhost ~]# php -r "unlink('composer-setup.php');"
```

#### 0.2.4 其他注意事项

##### 0.2.4.1 mysql远程连接配置

​		注意首先去宝塔上修改mysql的密码

![image-20230105121035944](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230105121035944.png)

```tex
mysql> grant all privileges on *.* to root@'%' identified by "password";
Query OK, 0 rows affected, 1 warning (0.01 sec)
mysql> flush privileges;
Query OK, 0 rows affected (0.01 sec)
```

##### 0.2.4.2 关闭防火墙

```tex
#查看防火墙状态
[root@localhost ~]# systemctl status firewalld
#开启防火墙
[root@localhost ~]# systemctl start firewalld
#关闭防火墙
[root@localhost ~]# systemctl stop firewalld
#开启防火墙
[root@localhost ~]# service firewalld start
#若遇到无法开启
#先用：
[root@localhost ~]# systemctl unmask firewalld.service
#然后：
[root@localhost ~]# systemctl start firewalld.service
#开放端口
[root@localhost ~]# firewall-cmd --zone=public --add-port=80/tcp --permanent
#查询端口号80是否开启:
[root@localhost ~]# firewall-cmd --query-port=80/tcp
#重启防火墙:
[root@localhost ~]# firewall-cmd --reload
#查询有哪些端口是开启的:
[root@localhost ~]# firewall-cmd --list-port
```

#### 0.2.5 一些基础概念

​		根据关键词理解事件，异步-协程并行，网络通信引擎;

##### 0.2.5.1 什么是事件

​		就是某一个动作发生的时候就可以称之为事件，例子：前台js的鼠标点击事件

##### 0.2.5.2 异步-协程并行

​		暂时可以理解为只是一种程序执行的模式，后面会详细解释

##### 0.2.5.3 网络通信引擎

​		网络就是程序之间的数据交流的连接，从而达到资源共享和通信的目的。通信是人与人之间通过某种媒体进行的信息交流与传递。网络通信是通过网络将各个鼓励的设备进行连接，通过信息交换实现人与人，人与计算 机，计算机u计算机兼职的通信，网络通信最终要的就是网络通信协议。

#### 0.2.6 swoole初体验

##### 0.2.6.1 客户端与服务端

实验环境注意事项：

```tex
我们可以根据手册所写的代码进行简单案例的实践
注意在练习的时候建议设置可以关闭防火墙

下面是相关防火墙的命令：
$ sudo systemctl stop firewalld
$ sudo systemctl disable firewalld
$ sudo systemctl status firewalld
```

###### 0.2.6.1.1 以tcp服务为例解释

​		server端代码

```php
<?php
// 1. 创建swoole 默认创建的是一个同步的阻塞tcp服务
$host = "192.168.186.129"; // 0.0.0.0 代表接听所有
// 创建Server对象，监听 127.0.0.1:9501端口
// 默认是tcp
$serv = new Swoole\Server($host, 9501);
// 2. 注册事件
$serv->on('Start', function($serv){
echo "启动swoole 监听的信息tcp:$host:9501\n";
});
//监听连接进入事件
$serv->on('Connect', function ($serv, $fd) {
echo "Client: Connect.\n";
});
//监听数据接收事件
$serv->on('Receive', function ($serv, $fd, $from_id, $data) {
$serv->send($fd, "Server: ".$data);
});
//监听连接关闭事件
$serv->on('Close', function ($serv, $fd) {
echo "Client: Close.\n";
});
// 3. 启动服务器
// 阻塞
$serv->start(); // 阻塞与非阻塞
?>
```

##### 0.2.6.2 客户与服务(通俗解释)

​		需要理解什么是服务端和客户端 ： 首先它们是相对而言的，服务和客户不是固定的。 

​		服务主要是提供与服务这一方面： 就是能够帮助我们做一件某个事情， 比如 xxx恋爱秘籍 -》 就教我们恋爱技巧 

​		而客户呢就是我们自己 去看这本秘籍 ， 因为秘籍中提供了这些 恋爱 技巧的服务。 

​		后来你的成为恋爱大师，要去教别人 那么这个时候你成为了 服务方， 他人就是客户 

​		在这个过程中 

​		xxx恋爱秘籍与你 的关系： xxx秘籍提供恋爱技巧服务， 你是客户 

​		你恋爱大师 教 别人，那么你与别人的关系就是： 你是提供恋爱技巧服务，别人是客户

##### 0.2.6.3 计算中的client与server

​		这样的事情在计算中也是有的，其实我们每天都在接触和使用只是没有这个概念，比如php连接MySQL，我们的连接方式就是通过pdo 传递用户名，密码，MySQL服务ip，端口号3306的方式连接了； 这个过程中MySQL就是服务端，PHP就是客户端； 

​		对于计算机来说

```tex
计算在运行程序的时候会分配一个端口号来运行而一台计算机可分配的端口号有1-65535的范围，访问计算的方式就是通过在该计算的网络段去进行访问的。

在服务端与客户端通信中主要是根据与服务端提供的可访问的ip地址，以及端口号；

但是在通信的时候我们需要一个通信的方式 这个方式我们称之为协议；比如http协议；该协议方式的是最为常见的方式，而然除了这种方式以外还存在于其他的方式比如tcp，udp等，有时候为了通信的安全会选择对于
协议设置一些加密的手段

但是在服务中还存在于一些功能就是，服务就需要接收和主动向关联它的客户端发送一些信息；比如微信公众号的群发，同样客户端也会发送和接收稍息

在服务端的设置-》 因为服务端与客户端是一对多的方式，因此为了全部监听接收到稍息那么就需要配置监听为0.0.0.0；
```

```tex
对于服务端和客户端，在进行交互的时候主要是以事件的方式作为驱动，发送事件，接收事件，连接事件，关闭事件
```

##### 0.2.6.4 代码解释

​		对于连接服务来说主要是三大块的内容，`1；创建连接对象，2：server监听事件，3：启动`

##### 0.2.6.5 推荐扩展

​		推荐可以下载这个组件：`composer require eaglewu/swoole-ide-helper`

### 0.3 基础概念与长连接

#### 0.3.1 网络通信模型

##### 0.3.1.1 同步与异步

​		参考文章 : [阻塞与非阻塞的区别 - 麦飞 - 博客园 (cnblogs.com)](https://www.cnblogs.com/orez88/articles/2513460.html)

​		对于程序的执行流程来说，一般是从上而下的方式来执行， 除非遇到流程控制语句会有一些变化， 但是原则上都会遵循这个原则； 对于PHP来说在单线程的模式下那么一个进程在执行PHP代码的时候就会从第一行执 行到最后一行，那么这个过程我们就可以认为这是同步的，如果中间遇到了sleep()就不得不等待一下执行； 这种模式基本上可以满足于很多的应用场景； 

​		但是并不是绝对的比如：像微信 在网站上登入的时候就会发一条信息在自己的微信账号上提醒你登入了 ， 再比如一份订单生成了之后对于用户来说并不会立即收到货，而这个发货的过程我们就可以做成异步的任务由 库存模块处理 等等操作；

###### 0.3.1.1.1 代码示例

​		同步代码

```php
<?php
require './vendor/autoload.php';
// use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Helper\QuestionHelper;
$input = new ArgvInput;
$output = new ConsoleOutput;
$helper = new QuestionHelper;


while (true) {

    $question = new Question('请输入你要创建的任务 : ');
    $info = $helper->ask($input, $output, $question);
    // var_dump();
    echo "完成".$info."任务\n";

    // 队列 -》 任务投递
    file_put_contents('task.txt', $info."\n", 8);

    $question = new Question('是否停止 : Y / N : ');
    $info = $helper->ask($input, $output, $question);

    if ($info == "Y") {
        break;
    }
}
```

​		运行结果

![image-20230108175110140](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230108175110140.png)

​		异步代码

```php
<?php
while (true) {
    $data = file_get_contents('task.txt');
    if ($data != '') {
        echo "完成: ".$data.'任务';
        file_put_contents('task.txt', '');
        echo "清空任务\n";

    } else {
        echo "没有任务\n";
    }
    sleep(2);
}
```

##### 0.3.1.2 阻塞与非阻塞

​		参考文章 : [阻塞与非阻塞的区别 - 麦飞 - 博客园 (cnblogs.com)](https://www.cnblogs.com/orez88/articles/2513460.html)

​		阻塞：意思就是在哪里等待，要等别人执行完成才能往下去执行； 

​		非阻塞：就是程序可以不用等待执行的结果， 就可以进行下一步的操作； 

​		那么在这个过程中对于对于order.php来说， 在我们向控制台输入了一个任务之后

```tex
程序就在等待任务下一个信息的输入，那么这个过程我们就可以认为它是同步阻塞的； 但是对于inventory.php来说，他并不需要等待order.php是否执行完才执行
```

```tex
对于它来说执行需要去看task.txt是否存在需要完成的任务，那么这个时候它相对于 order.php是一个异步非阻塞的；
```

​		对于阻塞与非阻塞的专有称呼有，同步阻塞，同步非阻塞，异步阻塞，异步非阻塞；而实际上我们并不需要关注同步非阻塞与异步阻塞，同步非阻塞在实现的过程中会存在一些复杂度可以采用异步非阻塞的方式巧妙地 解决好；而异步阻塞其实本身意义就并不大

#### 0.3.2 swoole中的异步回调

​		在swoole中存在这异步的回调模块-》 http://wiki.swoole.com/wiki/page/p-async.html 不过这个回调模块，在4.3版本中讲所有异步客户端模块已经迁移出去，在ext-async扩展中了，推荐使用的是协程客户端； 

​		当然我们可以去github上下载该扩展进行编译安装 https://github.com/swoole/ext-async, 这里我下载的是zip包

##### 0.3.2.1 安装步骤

```tex
[root@localhost file]# ls
ext-async-master.zip swoole-src-4.4.12 swoole-src-4.4.12.tar.gz
[root@localhost file]# unzip ext-async-master.zip
[root@localhost file]# ls
ext-async-master ext-async-master.zip swoole-src-4.4.12 swoole-src-4.4.12.tar.gz
[root@localhost file]# cd ext-async-master
[root@localhost ext-async-master]# phpize
checking target system type... x86_64-unknown-linux-gnu
configure: error: Cannot find php-config. Please use --with-php-config=PATH
[root@localhost ext-async-master]# find / -name php-config
/www/server/php/72/src/scripts/php-config
/www/server/php/72/bin/php-config
[root@localhost ext-async-master]# ./configure --with-php-config=/www/server/php/72/bin/php-config
[root@localhost ext-async-master]# make -j 4
[root@localhost ext-async-master]# sudo make install
Installing shared extensions: /www/server/php/72/lib/php/extensions/no-debug-non-zts-20170718/
[root@localhost ext-async-master]# find / -name php.ini
/www/server/php/72/etc/php.ini
[root@localhost ext-async-master]# vi /www/server/php/72/etc/php.ini 添加 extension=swoole_asynce.so
[root@localhost ext-async-master]# /etc/rc.d/init.d/php-fpm-72 restart
[root@localhost ext-async-master]# php -m | grep swoole
swoole
swoole_async
```

##### 0.3.2.2 代码体验

​		根据手册文档https://wiki.swoole.com/wiki/page/1524.html ; 

###### 0.3.2.2.1 异步客户端

```php
<?php
use Swoole\Async\Client;
$client = new Client(SWOOLE_SOCK_TCP);
$client->on("connect", function(Client $cli) {
	$cli->send("GET / HTTP/1.1\r\n\r\n");
});
$client->on("receive", function(Client $cli, $data){
    echo "Receive: $data";
    // $cli->send(str_repeat('A', 100)."\n");
    // sleep(1);
});
$client->on("error", function(Client $cli){
	echo "error\n";
});
$client->on("close", function(Client $cli){
	echo "Connection close\n";
});
$client->connect('127.0.0.1', 9501);
echo "你好";
?>
```

​		运行结果

###### 0.3.2.2.2 同步客户端

```php
<?php
$client = new swoole_client(SWOOLE_SOCK_TCP);
//连接到服务器
if (!$client->connect('127.0.0.1', 9501, 0.5)){
	die("connect failed.");
}
//向服务器发送数据
if (!$client->send("hello world")){
	die("send failed.");
}
//从服务器接收数据
$data = $client->recv();
if (!$data){
	die("recv failed.");
}
echo $data."\n";
//关闭连接
$client->close();
echo "其他事情\n";
?>
```

​		运行结果

#### 0.3.3 长连接

​		须知：服务器建立和关闭连接均是会有资源的消耗的 

​		长连接：其实就是一直连接的方式 

​		短连接：主要是建立一次连接，就没有了ajax 

​		ajax的特点就是主动向服务器发送请求，获取数据之后处理；那么我们为什么需要长连接呢？ 

​		比如：在做即时聊天的时候-》QQ，不管是客户端还是服务端；在用户发送了消息之后就需要能接收到并且又能够接收到服务器发送的消息； 

​		我们可以通过与ajax来实现这个功能；—》代码 ， 但是ajax在实现代码的时候因为它的特点只能是主动请求, 因此如果我们需要用ajax来实现的话那么这个时候就不得不让ajax进行轮询；

##### 0.3.3.1 实战小练习之Ajax聊天项目

​		前端代码

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <!-- jQuery静态库。-->
        <script src="jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function (){
                var message = "";
                setInterval(function(){
                    getdata();
                },500);
                let count = 0;
                let show = 0;
                function getdata() {
                    $.ajax({
                        url:'getdata.php',
                        type:'get',
                        dataType:'json',
                        success:function(msg){
                            console.log(msg);
                            if (msg['count'] <= show) {
                                return ;
                            }
                            for (var i = show; i < msg['count']; i++) {
                                  message += "<p>"+ msg['data'][i]['username'] + "：" + msg['data'][i]['message']+"</p>";
                                  $("#show_message").html(message);
                                  show++;
                            }
                        }
                    });
                }
                $("#ckq").keydown(function(e){    //按下键盘触发函数
                    if(e.keyCode==13){            //如果按下的是回车
                        var user = $('input:radio:checked').val();
                        $.ajax({
                            url: "chat.php",
                            type: "post",
                            async: false,
                            data: {"in":$("#ckq").val(),"username":user}
                        });
                        getdata();
                        $("#ckq").val("");
                    }
                });

            })
        </script>
    </head>
    <body>
        <p id="message">小董与晴晴的对话：</p>
        选择身份：
        <input type="radio" name="sex" value="小董" checked>小董
        <input type="radio" name="sex" value="晴晴">晴晴
        <input type="radio" name="sex" value="小鱼" >小鱼
        <input type="radio" name="sex" value="其他" >其他
        <br>
        <div id="show_message"></div>
        <input type="text" id="ckq" name="in">
    </body>
</html>
```

​		后端代码之连接数据库

```php
<?php
/**
 * 数据库DAO -->>> 对数据库进行操作的类
 */
class Db{
    /**
     * 连接数据的地址
     * @var string
     */
    CONST DRIVER_CLASS = 'mysql:host=localhost;dbname=join';

    /**
     * 数据库的用户名
     * @var string
     */
    CONST USERNAME = 'root';

    /**
     * 数据库的密码
     * @var string
     */
    CONST PASSWORD = 'root';

    /**
     * 数据库连接出错
     * @var string|array
     */
    private $error = '没有异常';

    /**
     * 连接数据库驱动
     * @var PDO
     */
    private $pdo;

    public function __construct(){
        try {
            // 初始化执行数据库类
            $this->pdo = new PDO(self::DRIVER_CLASS, self::USERNAME, self::PASSWORD);
            $this->pdo->query('SET NAMES UTF8');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException  $e) {
            // throw new \Exception($e->getMessage(), 500);
            return $e->getMessage();
        }
    }

    /**
     * 读操作 -->> 查询
     * @param  string $sql 查询sql
     * @return array       执行结果
     */
    public function query($sql){
        try {
            $result = $this->pdo->query($sql);
            $data = [];
            foreach($result as $key => $value){
                $data[] = $value;
            }
            return $data;
            // return (count($data) <= 1) ? $data[0] : $data ;

        } catch (PDOException  $e) {
            return $e->getMessage();
        }
    }

    public function call($sql, $select_param = null){
        $stmt = $this->pdo->prepare($sql);
        if ($stmt->execute()) {
            if (isset($select_param)) {
                return $this->pdo->query($select_param)->fetchAll();
            }
            return true;
        } else {
            return false;
        }
    }
    /**
     * 写操作 -->> 增删改
     * @param  string $sql 查询sql
     * @return array       执行结果
     */
    public function execute($sql){
        try {
            return $this->pdo->exec($sql);
        } catch (PDOException  $e) {
            // throw new \Exception($e->getMessage(), 500);
            return $e->getMessage();
        }
    }
    //------------------
    //属性get | set 方法
    //------------------

    /**
     * 获取系统错信息
     */
    public function getError(){
        return $this->error;
    }
    public function write($data){
        file_put_contents("log.txt",$data."\n",FILE_APPEND);
    }
}
$db = new Db;
```

​		后端代码之获取数据

```php
<?php
    include ("db.php");
    // $result = mysql_query("select message,username from mymessage");
    // $results = array();
    // while ($row = mysql_fetch_assoc($result)) {   //mysql_fetch_assoc() 函数从结果集中取得一行作为关联数组，返回根据从结果集取得的行生成的关联数组!，如果没有更多行，则返回 false。
    //     $results[] = $row;
    // }
    $jsonResult =  json_encode([
        'data' => $db->query("select message,username from mymessage"),
        'count' => ($db->query("select count(*) from mymessage"))[0][0]
    ]);
    echo $jsonResult;
?>

```

​		后端代码之写入聊天记录

```php
<?php
include ("db.php");
$sql = "INSERT INTO mymessage (message,username) VALUES('$_POST[in]','$_POST[username]')";
$db->execute($sql);
?>
```

​		数据库脚本

```tex
CREATE TABLE `mymessage` (
  `messageID` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL,
  `message` char(30) NOT NULL,
  PRIMARY KEY (`messageID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=gbk
```

​		运行效果

![image-20230108200442439](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230108200442439.png)

##### 0.3.3.2 出现的问题

​		可以看到前台不断的通过ajax请求与后台获取数据； 并进行响应和输出；

​		如果说是单台服务器并且只是单个连接， 其实问题不打 ； 

​		但是如果说链接很多那么服务器就会存在这很大的压力； 

​		在这个过程中服务器会不断 的创建连接关闭连接会消耗很多不必要的资源； =》 这个时候短连接就无法满足与我们项目的功能的需要 因此就需要长连接的帮助了；

​		其实还存在一个问题服务端也需要主动发送消息给客户端；

#### 0.3.4 心跳检测

​		在实际的生产环境中，更多的情况是在于用户通过内网的服务端 访问 上线部署的外网服务端；然而在这个过程中用户的服务端口可能因为一些网络方面的因素会造成与外网的服务器断开连接 -》 最典型的例子 “英雄联 盟突然掉线了， QQ的登入状态”； 这些在线上的过程都是属于长连接的类型，那么就会存在于这些问题；那么这个时候我们应该如何检测呢？？（当然除了断开连接还有一些其他问题，比如超时，阻塞等问题 之后解 释） 

​		首先须知： 对于服务端来说信息“基本”是公开的 比如ip地址，状态基本是在运行状态； 那么客户端呢？实际上客户端并不是这么一回事，他的运行状态，ip地址不是固定的； 

​		而对于长连接这种断开的问题；主要的点就在于服务端会保存客户端会话的有效性以及平台上监控所有客户端的网络状况；对于这种功能的实现我们可以通过两种方式实现

```tex
1. 轮询机制
2. 心跳机制
```

​		轮询：概括来说是服务端定时主动的去与要监控状态的客户端（或者叫其他系统）通信，询问当前的某种状态，客户端返回状态信息，客户端没有返回或返回错误、失效信息、则认为客户端已经宕机，然后服务端自己 内部把这个客户端的状态保存下来（宕机或者其他），如果客户端正常，那么返回正常状态，如果客户端宕机或者返回的是定义的失效状态那么当前的客户端状态是能够及时的监控到的，如果客户端宕机之后重启了那 么当服务端定时来轮询的时候，还是可以正常的获取返回信息，把其状态重新更新。 

​		心跳：最终得到的结果是与轮询一样的但是实现的方式有差别，心跳不是服务端主动去发信息检测客户端状态，而是在服务端保存下来所有客户端的状态信息，然后等待客户端定时来访问服务端，更新自己的当前状 态，如果客户端超过指定的时间没有来更新状态，则认为客户端已经宕机或者其状态异常。

​		心跳机制与轮询的比较，在我们的应用中，采用的是心跳，这样一是避免服务端的压力，二是灵活好控制，上一篇文章中提到过，我们的外网服务端（服务端）不知道内网服务端（客户端）的地址，有虽然有保存客户 端的socket会话，但是客户端宕机会话就失效了。所以只能等着他主动来报告状态。

​		注意：在客户端向服务端发送的标识我们可以称之为叫心跳包

##### 0.3.4.1 什么是心跳包

​		从客户端到服务器这条巨大的链路中会经过无数的路由器，每个路由器都可能会检测多少秒时间内没有数据包，则会自动关闭连接的节能机制。为了让这个可能会出现的节能机制失效,客户端可以设置一个定时器，每隔 固定时间发送一个随机字符一 字节的数据包，这种数据包就是心跳包。

#### 0.3.5 swoole中的心跳检测

​		参考文章：[Swoole Heartbeat 心跳 - 简书 (jianshu.com)](https://www.jianshu.com/p/69bc9c7fe5f1)

​		在swoole中我们可以通过set的方式来设置参数值；

* heartbeat_check_interval 设置服务器定时检测在线列表的时间间隔 
* heartbeat_idle_time 设置连接最大的空闲时间，如果最后-一个心跳包的时间与当前时间只差超过设定值则认为连接 失效。

​		建议heartbeat_idle_time 比heartbeat_check_interval 的值多两倍多，两倍是为了进行容错允许丢包，多一点儿是考虑到网络延时的情况，这个可以根据实际的业务情况调整容错率。

```php
<?php
//创建Server对象，监听 127.0.0.1:9501端口
$serv = new Swoole\Server("127.0.0.1", 9501);
$serv->set([
// https://wiki.swoole.com/wiki/page/283.html
// https://wiki.swoole.com/wiki/page/284.html
//心跳检测,每三秒检测一次，10秒没活动就断开
'heartbeat_idle_time'=>10,//连接最大的空闲时间
'heartbeat_check_interval'=>3 //服务器定时检查
]);
//监听连接进入事件
$serv->on('Connect', function ($serv, $fd) {
echo "Client ".$fd.": Connect.\n";
});
//监听数据接收事件
$serv->on('Receive', function ($serv, $fd, $from_id, $data){
$serv->send($fd, "Server: ".$data);
});
//监听连接关闭事件
$serv->on('Close', function ($serv, $fd) {
echo "Client: ".$fd."Close.\n" ;
});
echo "启动swoole tcp server 访问地址 127.0.0.1:9501 \n";
//启动服务器
$serv->start();
?>
```

​		client

```php
<?php
    use Swoole\Async\Client;
    $client = new Client(SWOOLE_SOCK_TCP);
    $client->on("connect", function(Client $cli) {
    	$cli->send("GET / HTTP/1.1\r\n\r\n");
    });
    $client->on("receive", function(Client $cli, $data){
    	echo "Receive: $data";
    });
    $client->on("error", function(Client $cli){
    	echo "error\n";
    });
    $client->on("close", function(Client $cli){
    	echo "Connection close\n";
    });
    $client->connect('127.0.0.1', 9501);
    $r = 0;
    while (true) {
    	sleep(1);$r++;echo $r."\n";
    }
?>
```

​		效果：根据如上的代码来说就是服务端会检测客户端的访问，如果说服务端在10秒内没有收到信息，就端口客户端的连接

##### 0.3.5.1 swoole客户端下的处理

​		可以看到如上的效果就是服务端，就会通过检测发现用户在10秒内没有收到client的请求的时候就断开了连接；我们就可以通过让客户端定时的向服务端发送一个心跳包，提醒服务端；双方是建立了连接的关系的 

​		定时器可以放在服务器中也可以放在客户端中， 在这个案例中的话我们主要是放在客户端中；https://wiki.swoole.com/wiki/page/480.html

​		可以看到swoole的定时可以直接定位到毫秒级别，相比起linux的定时器更加的精确（linux定时器是 精确到分）；而在其中swoole提供的两个定时器类型有

* swoole_timer_tick : 每隔固定时间执行某一个方法，会返回定时器id 
* swoole_timer_after : 在规定的时间后执行，只执行一次，会返回定时器id 
* swoole_timer_clear ：清空定时器，条件是定时器的id

###### 0.3.5.1.1 代码

```php
<?php
use Swoole\Async\Client;
$client = new Client(SWOOLE_SOCK_TCP);
$client->on("connect", function(Client $cli) {
$cli->send("GET / HTTP/1.1\r\n\r\n");
});
$client->on("receive", function(Client $cli, $data){
echo "Receive: $data";
});
$client->on("error", function(Client $cli){
echo "error\n";
});
$client->on("close", function(Client $cli){
echo "Connection close\n";
});
$client->connect('127.0.0.1', 9501);
//每隔2000ms触发一次
swoole_timer_tick(4000, function ($timer_id) use ($client) {
echo "请求信息\n";
$client->send('1');
});
?>
```

​		当然服务端我们可以进行修改一下， 在服务端中可以打印出客户端的信息

```php
<?php
//创建Server对象，监听 127.0.0.1:9501端口
$serv = new Swoole\Server("127.0.0.1", 9501);
$serv->set([
// https://wiki.swoole.com/wiki/page/283.html
// https://wiki.swoole.com/wiki/page/284.html
//心跳检测,每三秒检测一次，10秒没活动就断开
'heartbeat_idle_time'=>6,//连接最大的空闲时间
'heartbeat_check_interval'=>3 //服务器定时检查
]);
//监听连接进入事件
$serv->on('Connect', function ($serv, $fd) {
echo "Client ".$fd.": Connect.\n";
});
//监听数据接收事件
$serv->on('Receive', function ($serv, $fd, $from_id, $data) {
// 接收客户端的想你想
echo "接收到".$fd."信息的".$data."\n";
$serv->send($fd, "Server: ".$data);
});
//监听连接关闭事件
$serv->on('Close', function ($serv, $fd) {
echo "Client: ".$fd."Close.\n" ;
});
echo "启动swoole tcp server 访问地址 127.0.0.1:9501 \n";
//启动服务器
$serv->start();
?>
```

#### 0.3.6 udp与tcp

​		首先我们可以先创一个upd的服务端 找到官网： https://wiki.swoole.com/wiki/page/26.html

```php
<?php
//创建Server对象，监听 127.0.0.1:9502端口，类型为SWOOLE_SOCK_UDP
$serv = new swoole_server("127.0.0.1", 9502, SWOOLE_PROCESS, SWOOLE_SOCK_UDP);
//监听数据接收事件
$serv->on('Packet', function ($serv, $data, $clientInfo) {
$serv->sendto($clientInfo['address'], $clientInfo['port'], "Server ".$data);
var_dump($clientInfo);
});
//启动服务器
$serv->start();
?>
```

​		我们可以看到在官网中是存在这一个问题， 那就是upd与tcp不同，就是upd可以不用客户进行connect，就可以直接访问服务端的信息；而在服务端中就会存在只有对应接收信息的onPacket方法； 

​		创建upd客户端，对于客户端的创建方式实际上并不是特别难，与tcp的同步客户端创建有一些类似，只是需要注意传递的常量 https://wiki.swoole.com/wiki/page/26.html

```php
<?php
$client = new swoole_client(SWOOLE_SOCK_UDP);
// 为什么不需要连接
//向服务器发送数据
$client->sendTo("127.0.0.1", 9502, "你好我是udp客户端");
//从服务器接收数据
$data = $client->recv();
echo "其他事情\n";
?>
```

​		测试

​		注意如果udp没有执行 $serv->sendto($clientInfo['address'], $clientInfo['port'], "Server ".$data); 也就是向客户端发送消息那么这个时候就会，客户端阻塞

### 0.4 网络协议与tcp问题

#### 0.4.1 初识网络协议

​		TCP/IP协议族是一个四层协议系统，自底而上分別是数据链路层、网络层、传输层和应用层。每一层完成不同的功能，且通过若干协议来实现，上层协议使用下层协议提供的服务

![image-20230109235127908](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230109235127908.png)

##### 0.4.1.1 数据链路层

​		数据链路层实现了网卡接口的网络驱动程序，以处理数据在物理媒介上的传输。 

​		数据链路展两个常用的协议是ARP协议（Address Resolve Protocol,地址解析协议）和RARP协议（Reverse Address Resolve Protocol,逆地址解析协议）.它们实现了 IP地址和机器物理地址（MAC地址）之间的相互转换， 

​		当数据交换时上层网络层使用IP地址寻找一台机器，而数据链路层使用物理地址寻找一台机器，因此网络层必须先将目标标机器的IP地址转化成其物理地址,才能使用数据链路提供的服务，这就 ARP协议的用途

##### 0.4.1.2 网络层

​		网络层实现数据包的选路和转发，网络层最核心的协议是IP协议（Internet Protocol,因特网协议）。IP协议根据数据包的目的IP地址来决定如何投递它。

​		网络层另外一个重要的协议足ICMP协议（Internet Control Message Protocol»因特网控制制报文协议）。它是IP协议的重要要补充，主要用于检测网络连接，比如可以通过发送报文来检测目标是否可达

##### 0.4.1.3 传输层

​		网络层当中的IP协议为了解决传输当中的路径选择的问题，只需要按照此路径传输数据即可，传输层利用udp或者tcp协议以及网络层提供的路径信息为基础完成实际的数据传输，所以称为传输层

##### 0.4.1.4 应用层

​		应用层负责处理应用程序的逻辑。数据链路层、网络层和传输层负责处理网络通讯细节，这部分必须既稳定又高效,因此它们都在内核空间中实现，应用层则负责在用户户空间实现，因为为它负责处理众多逻辑。

​		应用层协议举例：

* FTP文件传输协议是TCP/IP网络上两台计算机传送文件的协议 

* SMTP（SimpleMailTransferProtocol）即简单邮件传输协议

#### 0.4.2 认识TCP

##### 0.4.2.1 介绍tcp与upd

​		**TCP(Transmission Control Protocol传输控制协议): **是一 种面向连接的、可靠的、基于字节流的传输层通信协议，使用三次握手协议建立连接、四次挥手断开连接。面向连接意味着两个使用TCP的应用(通常是一个 客户端和一 个服务器)在彼此交换数据包之前必须先建立一一个TCP连接。在一一个TCP连接中，仅有两方进行彼此通信，广播和多播不能用TCP。TCP协议的作用是，保证数据通信的完整性和可靠性，防止丢包。TCP把 连接作为最基本的对象,每-条TCP连接都有两个端点, 这种端点我们叫作套接字(socket),端口号拼接到IP地址即构成了套接字。

​		**UDP(User Datagram Protocol用户数据报协议): ** 是OSI(Open System Interconnection开放式系统互联)参考模型中- -种无连接的传输层协议，提供面向事务的简单不可靠信息传送服务。UDP协议的主要作用是将网 络数据流量压缩成数据包的形式。

###### 0.4.2.1.1 tcp与udp区别

1. TCP提供的是面向连接的、可靠的数据流传输; UDP提供的是非面向连接的、不可靠的数据流传输。

2. TCP提供可靠的服务，通过TCP连接传送的数据，无差错、不丢失、不重复，按序到达; UDP尽最大努力交付，即不保证可靠交付。

3. TCP面向字节流; UDP面向报文。

4. TCP连接只能是点到点的; UDP支持-对一、一对多 多对一和多对多的交互通信。 

5. UDP具有较好的实时性，工作效率比TCP高，适用于对高速传输和实时性有较高的通信或广播通信。

6. TCP对系统资源要求较多，UDP对系统资源要求较少。TCP首部有20字节; UDP的首部只有8个字节。

7. TCP的逻辑通信信道是全双工的可靠信道; UDP的逻辑通信信道是不可靠信道。

##### 0.4.2.2 TCP通信过程(三次握手，四次挥手)

​		文章参考来源:[通俗易懂理解TCP协议三次握手和四次挥手及其常见问题_impact_factor的博客-CSDN博客_三次握手四次挥手](https://blog.csdn.net/impact_factor/article/details/119394391)

###### 0.4.2.2.1 通俗解释三次握手

​		将小明当作客户端，小红当作服务器端，两人写信告白：

​		**第一次握手：**小明告诉小红：我喜欢你。
​		**第二次握手：**小红告诉小明：我知道了，我也喜欢你。
​		此时小红并不确定小明是否收到了告白信，直到
​		**第三次握手：**小明回信：我也知道了，我们在一起吧。此时才真正建立连接。

###### 0.4.2.2.2 三次握手示意图

![image-20230110001626471](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230110001626471.png)

| 标记位 | 解释                                                         |
| ------ | ------------------------------------------------------------ |
| SYN    | 请求号标记位                                                 |
| ACK    | 确认号标记位                                                 |
| seq    | 序号，代表请求方将会发送的数据的第一个字节编号               |
| ack    | 返回的确认号，代表接收方收到收据后（也就是前面说的seq），代表希望对方下一次传输数据的第一个字节编号 |

| 状态位      | 解释                                                         |
| ----------- | ------------------------------------------------------------ |
| CLOSED      | client处于关闭状态                                           |
| LISTEN      | server处于监听状态，等待client连接                           |
| SYN-RCVD    | 表示server接受到了SYN报文，当收到client的ACK报文后，它会进入到ESTABLISHED状态 |
| SYN-SENT    | 表示client已发送SYN报文，等待server的第2次握手               |
| ESTABLISHED | 表示连接已经建立                                             |

**详细解释**

* 第一次握手：客户端第一次发送一条连接请求数据，SYN = 1，ACK = 0就是代表建立连接请求，发送的具体数据第一个字节编号记为x，赋值seq。

* 第二次握手：服务端收到请求后，返回 客户端的SYN = 1,加上自己的确认号ACK=1,发送的具体数据第一个字节编号记为y，赋值seq，希望客户端下一次返回编号x + 1个字节为止的数据，记为ack = x + 1。

​		客户端得出客户端发送接收能力正常，服务端发送接收能力也都正常，但是此时服务器并不能确认客户端的接收能力是否正常

* 第三次握手：客户端收到服务端返回的请求确认后，再次发送数据，原封不动返回ACK = 1,这里就不需要再发送 SYN=1了，为什么呢？因为此时并不是跟服务端进行连接请求，而是连接确认，所以只需要返回ACK = 1代表确认，同样的，发送的具体数据第一个字节编号记为seq = x + 1，希望服务端下次传输的数据第一个字节编号记为ack = y + 1

###### 0.4.2.2.3 为啥要进行3次握手，两次不行嘛

​		假设client发出的第一个连接请求报文段，因为网络延迟，在连接释放以后的某个时间才到达server 。本来这是一个早已失效的连接请求，但server收到此失效的请求后，误认为是client再次发出的一个新的连接请求 。

​		于是server就向client发出确认报文段，同意建立连接。

​		如果不采用“3次握手”，那么只要server发出确认，新的连接就建立了 。

​		由于现在client并没有真正想连接服务器的意愿，因此不会理睬server的确认，也不会向server发送数据 。	

​		但server却以为新的连接已经建立，并一直等待client发来数据，这样，server的很多资源就白白浪费掉了

​		采用“三次握手”的办法可以防止上述现象发生 ，例如上述情况，client没有向server的确认发出确认，server由于收不到确认，就知道client并没有要求建立连接。

###### 0.4.2.2.4 通俗解释四次挥手

​		恋爱之后，小明和小红煲电话粥。依旧将小明当作客户端，小红当作服务器端。小明跟小红说话，

​		第一次挥手：小明说：我说完了。

​		第二次挥手：小红说：好的，我知道了，我还没说完。小红继续吧啦吧啦，说完之后

​		第三次挥手：小红告诉小明：我说完了。

​		第四次挥手：小明收到后告诉小红：好的，我知道了。等了2MSL之后小明挂断了。

​		如果此时小红说完，等了2MSL，小明一直不出声，这个时候就会重新说一次：我说完了。直到收到小明最后的回复，才挂断电话。

###### 0.4.2.2.5 四次挥手示意图

<img src="https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230110002643513.png" alt="image-20230110002643513" style="zoom:200%;" />

​		**状态位：** FIN = 1：代表要求释放连接

​		**详细解释**

* 第一次挥手：Client发送一个FIN，用来关闭Client到Server的数据传送，Client进入FIN_WAIT_1状态。
* 第二次挥手：Server收到FIN后，发送一个ACK给Client，确认序号为收到序号+1（与SYN相同，一个FIN占用一个序号），Server进入CLOSE_WAIT状态。
* 第三次挥手：Server发送一个FIN，用来关闭Server到Client的数据传送，Server进入LAST_ACK状态。
* 第四次挥手：Client收到FIN后，Client进入TIME_WAIT状态，接着发送一个ACK给Server，确认序号为收到序号+1，Server进入CLOSED状态，完成四次挥手

###### 0.4.2.2.6 为什么建立连接是三次握手，关闭连接确是四次挥手呢？

​		建立连接的时候， 服务器在LISTEN状态下，收到建立连接请求的SYN报文后，把ACK和SYN放在一个报文里发送给客户端。

​		而关闭连接时，服务器收到对方的FIN报文时，仅仅表示对方不再发送数据了但是还能接收数据，而自己也未必全部数据都发送给对方了，所以己方可以立即关闭，也可以发送一些数据给对方后，再发送FIN报文给对方来表示同意现在关闭连接，因此，己方ACK和FIN一般都会分开发送，从而导致多了一次。为了确保正确关闭连接，所以需要四次。

###### 0.4.2.2.7 TIME_WAIT状态有什么作用，为什么主动关闭方没有直接进入CLOSED状态释放资源？

​		防止连接关闭时四次挥手中的最后一次ACK丢失：如果主动关闭方进入CLOSED状态后，被动关闭方发送FIN包后没有得到ACK确认，超时后就会重传一个FIN包。如果客户端没有TIME_WAIT状态而直接进入CLOSED状态释放资源，下次启动新的客户端就可能使用了与之前客户端相同的地址信息，有两个危害，第一种是这个刚启动的新的客户端绑定地址成功时，就会收到了一个重传的FIN包，对新连接就会造成影响。第二种是如果该新客户端向相同的服务端发送SYN连接请求，但是此时服务端处于LAST_ACK状态，要求收到的是ACK而不是SYN，因此就会发送RST重新建立请求。

###### 0.4.2.2.8 为什么TIME_WAIT状态需要经过2MSL才能进入CLOSE状态?

​		MSL指的是报文在网络中最大生存时间。在客户端发送对服务端的FIN确认包ACK后，这个ACK包有可能到达不了，服务器端如果接收不到ACK包就会重新发送FIN包。所以客户端发送ACK后需要留出2MSL时间（ACK到达服务器器+服务器发送FIN重传包，一来一回）等待确认服务器端缺失收到了ACK包。也就是说客户端如果等待2MSL时间也没收到服务器端重传的FIN包，则就可以确认服务器已经收到客户端发送的ACK包

##### 0.4.2.3 TCP的特点

​		TCP是TCP/IP体系中非常复杂的一个协议，TCP最主要的特点有

1. TCP是面向连接的运输层协议。应用程序在使用TCP协议之前，必须先建立TCP连接。在传递数据完毕后，必须释放已建立的TCP连接。

2. 每一条TCP连接只能有两个端点，只能说点对点的。

3. TCP提供可靠交付的服务，通过TCP连接传送的数据，无差错，不丢失，不重复，并且按序到达。

4. TCP提供全双工通信。TCP允许通信双方的应用进程在任何时候都能发送数据。TCP连接的两端都设有发送缓存和接收缓存，用来临时存放双向通信的数据。

5. 面向字节流。TCP中的“流”指的是流入到进程或从进程流出的字节序列。“面向字节流”的含义是:虽然应用程序和TCP的交互是一-次一个数据块(大小不等)，但TCP把应用程序交下来的数据看成仅仅是一连 串的无结构 的字节流。TCP并不知道所传送的字节流的含义。TCP不保证接收方应用程序所收到的数据块和发送方应用程序所发出的数据块具有对应大小的关系。但接收方应用程序收到的字节流必须和发送方应用程序发出的字 节流完全一一样。 当然，接收方的应用程序必须有能力识别收到的字节流，把它还原成有意义的应用层数据。

![image-20230110000243006](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230110000243006.png)

###### 0.4.2.3.1 tcp特点简易版

​		具备可靠传输；对大流量拥有控制流量的权限；在进行高并发时，对拥塞具备一定的控制权。

##### 0.4.2.4 TCP问题之粘包

###### 0.4.2.4.1 数据发送问题

​		tcp在发送数据的时候因为存在数据缓存的关系，对于数据在发送的时候在 短时间内 如果连续发送很多小的数据的时候就会有可能一次性一起发送，还有就是对于大的数据就会分开连续发送多次

###### 0.4.2.4.2 实验环境演示

**示例代码**：

```php
<?php
// swoole_tcp_server.php
//创建Server对象，监听 127.0.0.1:9501端口
$serv = new Swoole\Server("127.0.0.1", 9501);
$serv->set([
    // https://wiki.swoole.com/wiki/page/283.html
    // https://wiki.swoole.com/wiki/page/284.html
    //心跳检测,每三秒检测一次，10秒没活动就断开
    'heartbeat_idle_time'=>6,//连接最大的空闲时间
    'heartbeat_check_interval'=>3 //服务器定时检查
]);
//监听连接进入事件
$serv->on('Connect', function ($serv, $fd) {
	echo "Client ".$fd.": Connect.\n";
});
//监听数据接收事件
$serv->on('Receive', function ($serv, $fd, $from_id, $data) {
	// 接收客户端的想你想
	echo "接收到".$fd."信息的".$data."\n";
	$serv->send($fd, "Server: ");
});
//监听连接关闭事件
$serv->on('Close', function ($serv, $fd) {
	echo "Client: ".$fd."Close.\n" ;
});
echo "启动swoole tcp server 访问地址 127.0.0.1:9501 \n";
//启动服务器
$serv->start();
// swoole_tcp_client.php
$client = new swoole_client(SWOOLE_SOCK_TCP);
//连接到服务器
if (!$client->connect('127.0.0.1', 9501, 0.5)){
	die("connect failed.");
}
//向服务器发送数据
for ($i=0; $i < 100; $i++) {
	$client->send("hello world");
}
//从服务器接收数据
$data = $client->recv();
if (!$data){
	die("recv failed.");
}
echo $data."\n";
//关闭连接
$client->close();
?>
```

**效果演示**：

![image-20230110001106012](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230110001106012.png)

**出现的现象**：

​		如上的情况就是多份数据变成了一份然后进行发送给了客服端，这种情况用一种专业的词：粘包 

​		所谓粘包就是，一个数据在发送的时候跟上了另一个数据的信息，另一个数据的信息可能是完整的也可能是不完整的； 

**出现的原因**

​		出现粘包现象的原因是多方面的，它既可能由发送方造成，也可能由接收方造成。发送方引|起的粘包是由TCP协议本身造成的, TCP为提高传输效率,发送方往往要收集到足够多的数据后才发送一包数据。 若连续几次发送 的数据都很少，通常TCP会根据优化算法把这些数据合成- -包后- 次发送出去，这样接收方就收到了粘包数据。 

​		接收方引起的粘包是由于接收方用户进程不及时接收数据，从而导致粘包现象。这是因为接收方先把收到的数据放在系统接收缓冲区，用户进程从该缓冲区取数据，若下一-包数据到达时前一-包数据尚未被用户进程取 走,则下一包数据放到系统接收缓冲区时就接到前一包数据之后，而用户进程根据预先设定的缓冲区大小从系统接收缓冲区取数据，这样就一-次取到了 多包数据

### 0.5 解决TCP问题之粘包问题

#### 0.5.1 演示粘包问题

##### 0.5.1.1 出现问题

​		tcp在发送数据的时候因为存在数据缓存的关系，对于数据在发送的时候在 短时间内 如果连续发送很多小的数据的时候就会有可能一次性一起发送，还有就是对于大的数据就会分开连续发送多次

##### 0.5.1.2 演示之一起发

​		代码

```php
<?php
// swoole_tcp_server.php
//创建Server对象，监听 127.0.0.1:9501端口
$serv = new Swoole\Server("127.0.0.1", 9501);
$serv->set([
    // https://wiki.swoole.com/wiki/page/283.html
    // https://wiki.swoole.com/wiki/page/284.html
    //心跳检测,每三秒检测一次，10秒没活动就断开
    'heartbeat_idle_time'=>6,//连接最大的空闲时间
    'heartbeat_check_interval'=>3 //服务器定时检查
]);
//监听连接进入事件
$serv->on('Connect', function ($serv, $fd) {
echo "Client ".$fd.": Connect.\n";
});
//监听数据接收事件
$serv->on('Receive', function ($serv, $fd, $from_id, $data) {
// 接收客户端的想你想
echo "接收到".$fd."信息的".$data."\n";
$serv->send($fd, "Server: ");
});
//监听连接关闭事件
$serv->on('Close', function ($serv, $fd) {
echo "Client: ".$fd."Close.\n" ;
});
echo "启动swoole tcp server 访问地址 127.0.0.1:9501 \n";
//启动服务器
$serv->start();
// swoole_tcp_client.php
$client = new swoole_client(SWOOLE_SOCK_TCP);
//连接到服务器
if (!$client->connect('127.0.0.1', 9501, 0.5))
{
die("connect failed.");
}
//向服务器发送数据
for ($i=0; $i < 100; $i++) {
$client->send("hello world");
}
//从服务器接收数据
$data = $client->recv();
if (!$data)
{
die("recv failed.");
}
echo $data."\n";
//关闭连接
$client->close();
?>
```

​		现象

![image-20230117143351969](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117143351969.png)

​		如上的情况就是多份数据变成了一份然后进行发送给了客服端，这种情况用一种专业的词：**粘包**。所谓粘包就是，**一个数据在发送的时候跟上了另一个数据的信息，另一个数据的信息可能是完整的也可能是不完整的**； 

###### 0.5.1.2.1 出现的原因

 		出现粘包现象的原因是多方面的，它既可能由发送方造成，也可能由接收方造成。发送方引|起的粘包是由TCP协议本身造成的, TCP为提高传输效率,发送方往往要收集到足够多的数据后才发送一包数据。 若连续几次发送 的数据都很少，通常TCP会根据优化算法把这些数据合成- -包后- 次发送出去，这样接收方就收到了粘包数据。 

​		接收方引起的粘包是由于接收方用户进程不及时接收数据，从而导致粘包现象。这是因为接收方先把收到的数据放在系统接收缓冲区，用户进程从该缓冲区取数据，若下一-包数据到达时前一-包数据尚未被用户进程取 走,则下一包数据放到系统接收缓冲区时就接到前一包数据之后，而用户进程根据预先设定的缓冲区大小从系统接收缓冲区取数据，这样就一-次取到了 多包数据。

##### 0.5.1.3 演示之多次发

```php
<?php
    // swoole_tcp_server.php
    // 监听数据接收事件
    $serv->on('Receive', function ($serv, $fd, $from_id, $data) {
        // 接收客户端的想你想
        // echo "接收到".$fd."信息的".$data."\n";
        echo "接收到".$fd."信息"."\n";
        $serv->send($fd, "Server: ");
    });
    // swoole_tcp_client.php
    $client->send(str_repeat('hello World', 100000));
?>
```

​		注意因为是一份大的数据因此看起来还是比较头疼的，因此在如上的代码中主要是根据请求的次数来判断；对于这样的问题就会造成的现象就是一次就有多次的请求；对于这个问题的处理就是我们可以采用分包的方式 处理； 

​		分包操作主要是由程序处理而并非tcp处理

#### 0.5.2 解决粘包问题的思路

##### 0.5.2.1 特殊字符方式(普通php代码方式)

1. 当时短连接的情况下，不用考虑粘包的情况

2. 如果发送数据无结构，如文件传输，这样发送方只管发送，接收方只管接收存储就ok,也不用考虑粘包

3. 如果双方建立长连接,需要在连接后一段时间内发送不同结构数据 接收方创建预处理线程，对接收到的数据包进行预处理，将粘连的包分开;

​		分包:是指在出现粘包的时候我们的接收方要进行分包处理。(在长连接中都会出现) 数据包的边界发生错位， 导致读出错误的数据分包，进而曲解原始数据含义。

​		粘包情况有两种，-种是粘在一起的包都是完整的数据包，另- -种情况是粘在一 起的包有不完整的包。 

​		通过定义一个特殊的符号，注意是客户端与服务端相互约定的； 然后呢下一步就是客户端每次发送的时候都会在后面添加这个数据，服务端做数据的字符串处理；

​		代码如下

```php
<?php
// swoole_tcp_client
for ($i=0; $i < 100; $i++) {
	$client->send("hello world\i");
}

// swoole_tcp_server
// 监听数据接收事件
$serv->on('Receive', function ($serv, $fd, $from_id, $data) {
    // 接收客户端的想你想
    var_dump(explode('\i', $data));
    // echo "接收到".$fd."信息的".$data."\n";
    // echo "接收到".$fd."信息"."\n";
    $serv->send($fd, "Server: ");
});
?>
```

​		运行效果

![image-20230117144811804](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117144811804.png)

> 注意:当然会存在一些"空数据" ， 实际上这个只是explode函数的分割问题，自行处理即可

##### 0.5.2.2 殊字符方式(swoole的解决方式)

​		swoole的解决办法就是通过EOF的方式处理；在swoole中提供了一个open_eof_split的选项: http://wiki.swoole.com./wiki/page/421.html

​		操作方式

```php
<?php
    $swoole->set([
        'open_eof_split' => true,
        'package_eof' => "\r\n",
    ]);
?>
```

> 注意：不过这种方式会存在着一个问题，那就是在内容中不能存在于package_eof的指定的字符串，因为对于swoole来说它就是通过遍历整个数据包的内容，查找EOF然后进行分割，同时也会对于CPU的资源消耗比较大。 其实这种方式并不是特别的推荐，但是也可以了解

##### 0.5.2.3 固定包头+包体协议(普通PHP代码处理)

​		对于这样的问题情况，实际上我们可以通过定义好发送消息的tcp数据包的格式，而这个对于服务端和客户端相互之间就遵守这个规范 

​		这种方式就是通过与在数据传输之后会在tcp的数据包中携带上数据的长度，然后呢服务端就可以根据这个长度，对于数据进行截取； 对于pack的解释 -》https://php.golaravel.com/function.pack.html 对于unpack的解释 -》https://php.golaravel.com/function.unpack.html 

> 须知：一个字节 = 8个二进制位

​		与内容结合

```php
<?php
// client 通过pack进行组包
for ($i=0; $i < 10; $i++) {
    // 组包
    // msg protocol
    // | ---- dataLen ---- | data |
    // | - fixed 2bytes - |
    $context = "123";
    $len = pack("n", strlen($context));
    var_dump($len);
    $send = $len . $context;
    // var_dump($send);
    $client->send($send);
}
// server
//监听数据接收事件
$serv->on('Receive', function ($serv, $fd, $from_id, $data){
    var_dump($data);
    $fooLen = unpack("n", substr($data, $count, 2))[1];
    var_dump($fooLen);
    $foo = substr($data, $count + 2, $fooLen);
    var_dump($foo);
    $serv->send($fd, "Server: ");
});
?>
```

​		不过如上的服务端还需要负责对于不同的数据进行解包的处理;

##### 0.5.2.4 固定包头+包体协议(swoole处理方式)

​		swoole的处理方式：

```php
<?php
$server->set([
    'open_length_check' => true,
    'package_max_length' => 2 * 1024 * 1024 ,
    'package_length_type' => 'N',
    'package_length_offset' => 0,
    'package_body_offset' => 4
]);
?>
```

相关配置介绍：

```tex
1. open_length_check： 打开包长检测特性
2. package_length_type： 长度字段的类型，固定包头中用一个4字节或2字节表示包体长度。
3. package_length_offset：从第几个字节开始是长度，比如包头长度为120字节，第10个字节为长度值，这里填入9（从0开始计数）
4. open_length_check： 打开包长检测特性
5. package_length_type： 长度字段的类型，固定包头中用一个4字节或2字节表示包体长度。
6. package_length_offset：从第几个字节开始是长度，比如包头长度为120字节，第10个字节为长度值，这里填入9（从0开始计数）
```

​		package_length_type 的参考 https://segmentfault.com/a/1190000008305573

​		实例测试：

```php
<?php
    // client
    $client = new swoole_client(SWOOLE_SOCK_TCP);
    $client->set([
        'open_length_check' => 1,
        'package_max_length' => 1024 * 1024 * 3,
        'package_length_type' => 'N',
        'package_length_offset' => 0,
        'package_body_offset' => 4,
    ]);
    //连接到服务器
    if (!$client->connect('127.0.0.1', 9501, 0.5)){
    	die("connect failed.");
    }
    //向服务器发送数据
    for ($i=0; $i < 10; $i++) {
        $send = "123";
        $send = (pack("N", strlen($send))) . $send;
        $client->send($send);
    }
    //从服务器接收数据
    $data = $client->recv();
    if (!$data){
    	die("recv failed.");
    }
    echo $data."\n";
    //关闭连接
    $client->close();
    echo "其他事情\n";

    // server
    // 创建Server对象，监听 127.0.0.1:9501端口
    $serv = new Swoole\Server("127.0.0.1", 9501);
    $serv->set([
        'open_length_check' => true,
        'package_max_length' => 1024 * 1024 * 4,
        'package_length_type' => 'N',
        'package_length_offset' => 0,
        'package_body_offset' => 4,
    ]);
    //监听连接进入事件
    $serv->on('Connect', function ($serv, $fd) {
    	echo "Client ".$fd.": Connect.\n";
    });
    //监听数据接收事件
    $serv->on('Receive', function ($serv, $fd, $from_id, $data) {
   	 	// 接收客户端的想你想
    	echo substr($data, 4)."\n";
    	$serv->send($fd, "Server: ");
    });
    //监听连接关闭事件
    $serv->on('Close', function ($serv, $fd) {
    	echo "Client: ".$fd."Close.\n" ;
    });
    echo "启动swoole tcp server 访问地址 127.0.0.1:9501 \n";
    //启动服务器
    $serv->start();
?>
```

### 0.6 进程与线程

#### 0.6.1 进程

##### 0.6.1.1 介绍

###### 0.6.1.1.1 专业解释

​		进程（Process）是计算机中的程序关于某数据集合上的一次运行活动，是系统进行资源分配和调度的基本单位，是操作系统结构的基础。在早期面向进程设计的计算机结构中，进程是程序的基本执行实体；在当 代面向线程设计的计算机结构中，进程是线程的容器。程序是指令、数据及其组织形式的描述，进程是程序的实体。 

###### 0.6.1.1.2 进程通俗解释

​		什么是进程，所谓进程其实就是操作系统中一个正在运行的程序，我们在一个终端当中，通过php，运行一个php文件，这个时候就相当于我们创建了一个进程，这个进程会在系统中驻存，申请属于它自己的内 存空间系统资源并且运行相应的程序 

###### 0.6.1.1.3 进程的组成

​		进程是一个实体。每一个进程都有它自己的地址空间，一般情况下，包括文本区域（text region）、数据区域（data region）和堆栈（stack region）。文本区域存储处理器执行的代码；数据区域存储变量和进程 执行期间使用的动态分配的内存；堆栈区域存储着活动过程调用的指令和本地变量。 

###### 0.6.1.1.4 进程的特征 

* 动态性：进程的实质是程序在多道程序系统中的一次执行过程，进程是动态产生，动态消亡的。 

* 并发性：任何进程都可以同其他进程一起并发执行 

* 独立性：进程是一个能独立运行的基本单位，同时也是系统分配资源和调度的独立单位； 

* 异步性：由于进程间的相互制约，使进程具有执行的间断性，即进程按各自独立的、不可预知的速度向前推进 

* 结构特征：进程由程序、数据和进程控制块三部分组成。 

​		多个不同的进程可以包含相同的程序：一个程序在不同的数据集里就构成不同的进程，能得到不同的结果；但是执行过程中，程序不能发生改变。

###### 0.6.1.1.5 进程状态示意图

![image-20230117150110809](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117150110809.png)

​		对于一个进程来说，它的核心内容分为两个部分，一个是它的内存，这个内存是这进程创建之初从系统分配的，它所有创建的变量都会存储在这一片内存环境当中 

​		一个是它的上下文环境我们知道进程是运行在操作系统的，那么对于程序来说，它的运行依赖操作系统分配给它的资源，操作系统的一些状态。

​		在操作系统中可以运行多个进程的，对于一个进程来说，它可以创建自己的子进程，那么当我们在一个进程中创建出若干个子进程的时候那么可以看到如图，子进程和父进程一样，拥有自己的内存空间和上下文环境

##### 0.6.1.2 为什么需要子进程

​		父进程：用来管理 子进程：用来工作

​		正常来说父进程停止了子进程也会停止运行

​		如何区分父进程和子进程呢？？

#### 0.6.2 线程

##### 0.6.2.1 介绍

​		线程，有时被称为轻量级进程(Lightweight Process，LWP），是程序执行流的最小单元。一个标准的线程由线程ID，当前指令指针(PC），寄存器集合和堆栈组成。另外，线程是进程中的一个实体，是被系统独立调度 和分派的基本单位，线程自己不拥有系统资源，只拥有一点儿在运行中必不可少的资源，但它可与同属一个进程的其它线程共享进程所拥有的全部资源。 

​		线程是程序中一个单一的顺序控制流程。进程内有一个相对独立的、可调度的执行单元，是系统独立调度和分派CPU的基本单位指令运行时的程序的调度单位。在单个程序中同时运行多个线程完成不同的工作，称为多线程。

##### 0.6.2.2 特点

​		在多线程OS中，通常是在一个进程中包括多个线程，每个线程都是作为利用CPU的基本单位，是花费最小开销的实体。线程具有以下属性。

​		1）轻型实体 线程中的实体基本上不拥有系统资源，只是有一点必不可少的、能保证独立运行的资源。 线程的实体包括程序、数据和TCB。线程是动态概念，它的动态特性由线程控制块TCB（Thread Control Block）描述。TCB包括以下信息： 				（1）线程状态。 

​				（2）当线程不运行时，被保存的现场资源。 

​				（3）一组执行堆栈。 

​				（4）存放每个线程的局部变量主存区。 

​				（5）访问同一个进程中的主存和其它资源。 用于指示被执行指令序列的程序计数器、保留局部变量、少数状态参数和返回地址等的一组寄存器和堆栈。 

​		2）独立调度和分派的基本单位。 在多线程OS中，线程是能独立运行的基本单位，因而也是独立调度和分派的基本单位。由于线程很“轻”，故线程的切换非常迅速且开销小（在同一进程中的）。 

​		3）可并发执行。 在一个进程中的多个线程之间，可以并发执行，甚至允许在一个进程中所有线程都能并发执行；同样，不同进程中的线程也能并发执行，充分利用和发挥了处理机与外围设备并行工作的能力。 

​		4）共享进程资源。

​		在同一进程中的各个线程，都可以共享该进程所拥有的资源，这首先表现在：所有线程都具有相同的地址空间（进程的地址空间），这意味着，线程可以访问该地址空间的每一个虚地址；此外，还可以访问进程所拥有 的已打开文件、定时器、信号量机构等。由于同一个进程内的线程共享内存和文件，所以线程之间互相通信不必调用内核。

#### 0.6.3 初识协程

​		协程与子进程一样，协程（coroutine）也是一种程序组件。相对子例程而言，协程更为一般和灵活，但在实践中使用没有子例程那样广泛。协程源自 Simula 和 Modula-2 语言，但也有其他语言支持。 

​		协程不是进程或线程，其执行过程更类似于子例程，或者说不带返回值的函数调用。 一个程序可以包含多个协程，可以对比与一个进程包含多个线程，dtrzw 

​		因而下面我们来比较协程和线程。我们知道多个线程相对独立，有自己的上下文，切换受系统控制；而协程也相对独立，有自己的上下文，但是其切换由自己控制，由当前协程切换到其他协程由当前协程来控制。

​		协程 和线程区别：协程避免了无意义的调度，由此可以提高性能，但也因此，程序员必须自己承担调度的责任，同时，协程也失去了标准线程使用多CPU的能力。

#### 0.6.4 进程与线程的关系与区别

![image-20230117150723973](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117150723973.png)

1. 地址空间:进程内的一个执行单元;进程至少有一个线程;它们共享进程的地址空间;而进程有自己独立的地址空间;因此线程可以读写同样的数据结构和变量，便于线程之间的通信。相反，进程间通信（IPC）很困难且 消耗更多资源。 
2. 资源拥有:进程是资源分配和拥有的单位,同一个进程内的线程共享进程的资源 
3. 进程是资源的分配和调度的一个独立单元，而线程是CPU调度的基本单元 
4. 二者均可并发执行. 
5. 进程的创建调用fork或者vfork，而线程的创建调用pthread_create，进程结束后它拥有的所有线程都将销毁，而线程的结束不会影响同个进程中的其他线程的结束 
6. 线程有自己的私有属性TCB，线程id，寄存器、硬件上下文，而进程也有自己的私有属性进程控制块PCB，这些私有属性是不被共享的，用来标示一个进程或一个线程的标志

#### 0.6.5 通俗理解计算机体系

##### 0.6.5.1 操作系统设计介绍

​		操作系统的设计，因此可以归结为三点：

* 以多进程形式，允许多个任务同时运行

* 以多线程形式，允许单个任务分成不同的部分运行

* 提供协调机制，一方面防止进程之间和线程之间产生冲突，另一方面允许进程之间和线程之间共享资源

##### 0.6.5.2 通俗解释

​		计算机的核心是CPU，它承担了所有的计算任务。它就像一座工厂，时刻在运行。

![image-20230117150910736](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117150910736.png)

​		假定工厂的电力有限，一次只能供给一个车间使用。也就是说，一个车间开工的时候，其他车间都必须停工。背后的含义就是，单个CPU一次只能运行一个任务

![image-20230117150928031](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117150928031.png)

​		进程就好比工厂的车间，它代表CPU所能处理的单个任务。任一时刻，CPU总是运行一个进程，其他进程处于非运行状态

![image-20230117150945107](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117150945107.png)

​		一个车间里，可以有很多工人。他们协同完成一个任务

![image-20230117151001659](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117151001659.png)

​		线程就好比车间里的工人。一个进程可以包括多个线程。

![image-20230117151023883](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117151023883.png)

​		车间的空间是工人们共享的，比如许多房间是每个工人都可以进出的。这象征一个进程的内存空间是共享的，每个线程都可以使用这些共享内存。

![image-20230117151046501](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117151046501.png)

​		可是，每间房间的大小不同，有些房间最多只能容纳一个人，比如厕所。里面有人的时候，其他人就不能进去了。这代表一个线程使用某些共享内存时，其他线程必须等它结束，才能使用这一块内存。

![image-20230117151059413](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117151059413.png)

​		一个防止他人进入的简单方法，就是门口加一把锁。先到的人锁上门，后到的人看到上锁，就在门口排队，等锁打开再进去。这就叫”互斥锁”（Mutual exclusion，缩写 Mutex），防止多个线程同时读写某一块内存区 域。

![image-20230117151124988](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117151124988.png)

​		还有些房间，可以同时容纳n个人，比如厨房。也就是说，如果人数大于n，多出来的人只能在外面等着。这好比某些内存区域，只能供给固定数目的线程使用。

![image-20230117151140590](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117151140590.png)

​		这时的解决方法，就是在门口挂n把钥匙。进去的人就取一把钥匙，出来时再把钥匙挂回原处。后到的人发现钥匙架空了，就知道必须在门口排队等着了。这种做法叫做”信号量”（Semaphore），用来保证多个线程不会 互相冲突。 

​		不难看出，mutex是semaphore的一种特殊情况（n=1时）。也就是说，完全可以用后者替代前者。但是，因为mutex较为简单，且效率高，所以在必须保证资源独占的情况下，还是采用这种设计。

![image-20230117151211427](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117151211427.png)

### 0.7 初识Swoole结构

#### 0.7.1 swoole进程结构

​		Swoole的高效不仅仅于底层使用c编写，他的进程结构模型也使其可以高效的处理业务，我们想要深入学习，并且在实际的场景当中使用必须了解，下面我们先看一下结构图：

![image-20230117182447425](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117182447425.png)

​		如上分为四层：master（主进程） Manger（管理进程） worker（工作进程） task（异步任务工作进程）

![image-20230117183436621](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117183436621.png)

##### 0.7.1.1 master（主进程）

​		第一层，Master进程，这个是swoole的主进程,这个进程是用于处理swoole的核心事件驱动的，那么在这个进程当中可以看到它拥有一个MainReactor[线程]以及若干个Reactor[线程]，swoole所有对于事件的监听都会在这 些线程中实现，比如来自客户端的连接，信号处理等。

![image-20230117183550002](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117183550002.png)

每一个线程都有自己的用途，下面多每个线程有一个了解

###### 0.7.1.1.1 MainReactor（主线程）

​		主线程会负责监听server socket，如果有新的连接accept，主线程会评估每个Reactor线程的连接数量。将此连接分配给连接数最少的reactor线程，做一个负载均衡。

###### 0.7.1.1.2 Reactor线程组

​		Reactor线程负责维护客户端机器的TCP连接、处理网络IO、收发数据完全是异步非阻塞的模式。 swoole的主线程在Accept新的连接后，会将这个连接分配给一个固定的Reactor线程，在socket可读时读取数据，并进行协 议解析，将请求投递到Worker进程。在socket可写时将数据发送给TCP客户端。

###### 0.7.1.1.3 心跳包检测线程（HeartbeatCheck）

​		Swoole配置了心跳检测之后，心跳包线程会在固定时间内对所有之前在线的连接发送检测数据包

###### 0.7.1.1.4 UDP收包线程（UdpRecv）

​		接收并且处理客户端udp数据包 

​		第一层，Master进程，这个是swoole的主进程,这个进程是用于处理swoole的核心事件驱动的，那么在这个进程当中可以看到它拥有一个MainReactor[线程]以及若干个Reactor[线程]，swoole所有对于事件的监听都会在这 些线程中实现，比如来自客户端的连接，信号处理等。

![image-20230117183550002](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117183550002.png)

​		每一个线程都有自己的用途，下面多每个线程有一个了解

##### 0.7.1.2 Manger（管理进程）

​		Swoole想要实现最好的性能必须创建出多个工作进程帮助处理任务，但Worker进程就必须fork操作，但是fork操作是不安全的，如果没有管理会出现很多的僵尸进程，进而影响服务器性能，同时worker进程被误杀或者由于程序的原因会异常退出，为了保证服务的稳定性，需要重新创建worker进程。

​		Swoole在运行中会创建一个单独的管理进程，所有的worker进程和task进程都是从管理进程Fork出来的。管理进程会监视所有子进程的退出事件，当worker进程发生致命错误或者运行生命周期结束时，管理进程会回收 此进程，并创建新的进程。换句话也就是说，对于worker、task进程的创建、回收等操作全权有“保姆”Manager进程进行管理。 

​		再来一张图梳理下Manager进程和Worker/Task进程的关系。

![image-20230117183943153](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117183943153.png)

###### 0.7.1.2.1 worker

​		worker 进程属于swoole的主逻辑进程，用户处理客户端的一系列请求，接受由Reactor线程投递的请求数据包，并执行PHP回调函数处理数据生成响应数据并发给Reactor线程，由Reactor线程发送给TCP客户端可以是异 步非阻塞模式，也可以是同步阻塞模式

###### 0.7.1.2.2 task

​		taskWorker进程这一进城是swoole提供的异步工作进程，这些进程主要用于处理一些耗时较长的同步任务，在worker进程当中投递过来。

#### 0.7.2  进程查看及流程梳理

​		当启动一个Swoole应用时，一共会创建2 + n + m个进程，2为一个Master进程和一个Manager进程，其中n为Worker进程数。m为TaskWorker进程数。 

​		默认如果不设置，swoole底层会根据当前机器有多少CPU核数，启动对应数量的Reactor线程和Worker进程。我机器为1核的。Worker为1。 

​		所以现在默认我启动了1个Master进程，1个Manager进程，和1个worker进程，TaskWorker没有设置也就是为0，当前server会产生3个进程。 

​		在启动了server之后，在命令行查看当前产生的进程

![image-20230117184138453](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117184138453.png)

​		这三个进程中，所有进程的根进程，也就是例子中的2123进程，就是所谓的Master进程；而2212进程，则是Manager进程；最后的2321进程，是Worker进程。 

​		当然我们也可以对于swoole的进程限制数量

```php
<?php
    // server
    $server->set([
    	'worker_num'=>1, //设置进程
    ]);
?>

```

​		效果

![image-20230117184221873](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117184221873.png)

#### 0.7.3  swoole进程函数

| 事件           | 哪种类型的进程 | 作用                                                         |
| -------------- | -------------- | ------------------------------------------------------------ |
| onstart        | master进程     | server启动在朱金城                                           |
| onshutdown     | master进程     | 此事件在server正常结束时发生                                 |
| onManagerStart | manager进程    | 当管理进程启动时调用它                                       |
| onManagerStop  | manager进程    | 当管理进程结束时调用它                                       |
| onWorkError    | manager进程    | 当worker/task_worker进程发生异常后会在manager进程内回调此函数 |
| onWorkerStart  | worker进程     | 此事件在Worker进程/Task进程启动时发生                        |
| onWorkerStop   | worker进程     | 此事件在worker进程终止时发生                                 |
| onConnect      | worker进程     | 有新的连接进入时，在worker进程中回调                         |
| onClose        | worker进程     | TCP客户端连接关闭后，在worker进程中回调此函数                |
| onReceive      | worker进程     | 接收到数据时回调此函数，发生在worker进程中                   |
| onPacket       | worker进程     | 接收到UDP数据包时回调此函数，发生在worker进程中              |
| onFinish       | worker进程     | 当worker进程投递的任务在task_worker中完成时，task进程会通过finish()方法将任务处理的结果发送给worker进程 |
| onWorkerExit   | worker进程     | 仅在开启reload_async特性后有效。异步重启特性                 |
| onPipeMessage  | worker进程     | 当工作进程收到由 sendMessage 发送的管道消息时会触发事件      |
| onTask         | Task进程       | 在task_worker进程内被调用。worker进程可以使用swoole_server_task函数向task_worker进程投递新的任务 |
| onWorkerStart  | Task进程       | 此事件在Worker进程/Task进程启动时发生                        |
| onPipeMessage  | Task进程       | 当工作进程收到由 sendMessage 发送的管道消息时会触发事件      |

​		如上的表格就是swoole的进程可以绑定的时间， 并不是所有的；比如在启动的时候做的操作，那么就会触发启动的事件，发送信息的时候会触发的事件； 

​		swoole对于不同的进程，在不同的情况下就定义了不同的事件，这里可以像大家介绍一下事件的使用与其效果，但是并不是所有的事件。 

​		我们可以看一下swoole在官方所提供的swoole运行流程图

![image-20230117184956509](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117184956509.png)

```tex
1. 服务器关闭程序终止时最后一次事件是onShutdown。
2. 服务器启动成功后，onStart/onManagerStart/onWorkerStart会在不同的进程内并发执 行，并不是顺序的。
3. 所有事件回调均在$server->start后发生，start之后写的代码是无效代码。
4. onStart/onManagerStart/onWorkerStart 3个事件的执行顺序是不确定的
```

​		Swoole的Reactor、Worker、TaskWorker之间可以紧密的结合起来，提供更高级的使用方式。

​		一个更通俗的比喻，假设Server就是一个工厂，那Reactor就是销售，接受客户订单。而Worker就是工人，当销售接到订单后，Worker去工作生产出客户要的东西。而TaskWorker可以理解为行政人员，可以帮助Worker干 些杂事，让Worker专心工作。

​		比如我们可以尝试对于swoole的进程更名称

```php
<?php
    //创建Server对象，监听 127.0.0.1:9501端口
    $serv = new Swoole\Server("127.0.0.1", 9501);
    $serv->set([
        'worker_num' => 1
    ]);
    $serv->on('start', function () {
        swoole_set_process_name("swoole:start");
        echo "设置swoole 进程 xxx swoole:start \n";
    });
    $serv->on('managerStart', function () {
        swoole_set_process_name("swoole:managerStart");
        echo "设置swoole 进程 xxx swoole:managerStart \n";
    });
    $serv->on('workerStart', function () {
        swoole_set_process_name("swoole:workerStart");
        echo "设置swoole 进程 xxx swoole:workerStart \n";
    });
    //监听连接进入事件
    $serv->on('Connect', function ($serv, $fd) {
        echo "Client: Connect.\n";
    });
    //监听数据接收事件
    $serv->on('Receive', function ($serv, $fd, $from_id, $data) {
        $serv->send($fd, "Server: ".$data);
    });
    //监听连接关闭事件
    $serv->on('Close', function ($serv, $fd) {
        echo "Client: Close.\n";
    });
    echo "启动swoole tcp server 访问地址 127.0.0.1:9501\n";
    //启动服务器
    $serv->start();
?>
```

![image-20230117185134502](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230117185134502.png)

​		建议大家在平时的工作中可以看看swoole手册中的高级部分，因为对于swoole的理解会更加好一点

swoole中的Reactor https://wiki.swoole.com/wiki/page/163.html 

* Reactor是管理tcp连接的， 并向worker进程投递请求，投递方式根据dispatch_mode配置。

* worker进程可以自己处理任务，也可以投递给taskworker进程来做

### 0.8 认识网络IO模型

#### 0.8.1 初体验stream_socket相关函数

##### 0.8.1.1 stream_socket相关函数介绍

| 函数                 | 作用                     | 参考地址                                                     |
| -------------------- | ------------------------ | ------------------------------------------------------------ |
| stream_socket_server | 创建一个socket           | https://www.php.net/manual/zh/function.stream-socket-server.php |
| stream_socket_accept | 用于接收连接             | https://www.php.net/manual/zh/function.stream-socket-accept.php |
| fread                | 函数读取文件             | https://www.w3school.com.cn/php/func_filesystem_fread.asp    |
| is_callable          | 检测是否存在可运行的函数 | https://www.php.net/manual/zh/function.is-callable.php       |
| call_user_func       | 调用某一个方法           | https://www.php.net/manual/zh/function.call-user-func.php    |

##### 0.8.1.2 示例代码

```php
<?php
    // 06/stream_socket_server.php
    $host = "tcp://0.0.0.0:9000";
    // 创建socket服务
    $server = stream_socket_server($host);
    echo $host."\n";
    // 建立与客户端的连接
    // 服务就处于一个挂起的状态 -》 等待连接进来并且呢创建连接
    // stream_socket_accept 是阻塞
    // 监听连接 -》 可以重复监听
    while (true) {
    $client = @stream_socket_accept($server);
    // sleep(3);
    var_dump(fread($client, 65535));
    fwrite($client, "server hellow");
    fclose($client);
    var_dump($client);
    }
?>
```

```php
<?php
    // 06/stream_socket_client.php
    // 是建立连接
    $client = stream_socket_client("tcp://127.0.0.1:9000");
    $new = time();
    // 给socket通写信息
    fwrite($client, "hello world");
    // 读取信息
    var_dump(fread($client, 65535));
    // 关闭连接
    fclose($client);
    echo "\n".time()- $new;
?>
```

#### 0.8.2 认识网络io模型

##### 0.8.2.1 阻塞式I/O模型(blocking i/o)

​		示意图：

![image-20230119002815470](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119002815470.png)

​		专业解释：在阻塞式I/O模型中，应用程序在从调用recvfrom开始到它返回有数据报准备好这段时间是阻塞的，recvfrom返回成功后，应用进程开始处理数据报

​		通俗解释：一个人在钓鱼，当没鱼上钩时，就坐在岸边一直等

##### 0.8.2.2 非阻塞式I/O模型(non-blocking i/o)

​		示意图：

![image-20230119003102269](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119003102269.png)

​		专业解释：在非阻塞式I/O模型中，应用程序把一个套接口设置为非阻塞就是告诉内核，当所请求的I/O操作 无法完成时，不要将进程睡眠，而是返回-一个错误，应用程序基于I/O操作函数将不断的轮询数据 是否已经准备好，如 果没有准备好，继续轮询，直到数据准备好为止

​		通俗解释：边钓鱼边玩手机，隔会再看看有没有鱼上钩，有的话就迅速拉杆

##### 0.8.2.3 I/O复用模型(i/o multiplexing)

​		示意图：

![image-20230119003201237](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119003201237.png)

​		专业解释：在I/O复用模型中，会用到select或poll函数或epoll函数(Linux2.6以后的内核开始支持)， 这两个函数也会使进程阻塞，但是和阻塞I/O所不同的的，这两个函数可以同时阻塞多个I/O操作，而且可以同时对多个读操作，多 个写操作的I/0函数进行检测，直到有数据可读或可写时，才填正调用I/O操作函数

​		通俗解释：放了一堆鱼竿,在岸边- -直守着这堆鱼竿，直到有鱼上钩

##### 0.8.2.4 信号驱动式I/O模型(signal-driven i/o)

​		示意图：

![image-20230119003254798](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119003254798.png)

​		专业解释：由POSIX规范定义，应用程序告知内核启动某个操作，并让内核在整个操作( 包括将数据从内核拷贝到应用程序的缓冲区)完成后通知应用程序。这种模型与信号驱动模型的主要区别在于:信号驱动I/O是由内核通知应用程 序何时启动一个I/O操作， 而异步I/O模型是由内核通知应用程序I/O操作何时完成

##### 0.8.2.5 异步I/O模型(asynchronous i/o)

​		示意图：

![image-20230119003342559](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119003342559.png)

​		专业解释：由POSIX规范定义，应用程序告知内核启动某个操作，并让内核在整个操作( 包括将数据从内核 拷贝到应用程序的缓冲区)完成后通知应用程序。这种模型与信号驱动模型的主要区别在于:信 号驱动I/O是由内核通知应用 程序何时启动一个I/O操作， 而异步I/O模型是由内核通知应用程序 I/O操作何时完成

##### 0.8.3 实战练习项目之手写workerman

##### 0.8.3.1 前期准备

​		为了更好地理解，网络编程， 写出高性能的服务； 那么首先我们需要了解在进程阻塞的网络服务器的实现过程； 这里我们以原生的代码诠释 

​		为了与项目更好的阅读，方便编辑这里使用一下composer来加载命名规则及其诠释代码的目录结构

```tex
io-
    - src
        - Blocking
        - NonBlocking
        - Multiplexing
        - SingnalDriven
        - Asynchronous
    - test
        - blocking
        - nonBlocking
        - multiplexing
        - singnal-driven
        - asynchronous
    - vendor
    - composer.json

```

​		构建项目：

```tex
$ io>composer init
Welcome to the Composer config generator

This command will guide you through creating your composer.json config.

Package name (<vendor>/<name>) [shineyork/io]: shineyork/io
Description []:
Author [, n to skip]: shineyork <shineyork@sixstaredu.com>
Minimum Stability []:
Package Type (e.g. library, project, metapackage, composer-plugin) []: library
License []: MIT

Define your dependencies.

Would you like to define your dependencies (require) interactively [yes]?
Search for a package:
Would you like to define your dev dependencies (require-dev) interactively [yes]?
Search for a package:
{
    "name": "shineyork/io",
    "type": "library",
    "license": "MIT",
    "authors": [
        {
            "name": "shineyork",
            "email": "shineyork@sixstaredu.com"
        }
    ],
    "require": {}
}
Do you confirm generation [yes]? yes
```

​		然后修一下composer.json即可

```json
{
    "name": "shineyork/io",
    "description": "io",
    "type": "library",
    "license": "MIT",
    "authors": [
        {
        	"name": "shineyork",
        	"email": "shineyork@sixstaredu.com"
        }
    ],
    "autoload":{
        "psr-4":{
        	"ShineYork\\Io\\" : "./src/"
        }
    },
	"require": {}
}
```

##### 0.8.3.2 阻塞io模型的实现（TCP服务）

```php
// io/src/Blocking/Worker.php
<?php
namespace ShineYork\Io\Blocking; 
// 这是等会自个要写的服务 
class Worker { 
    // 自定义服务的事件注册函数， 
    // 这三个是闭包函数 
    public $onReceive = null;
    public $onConnect = null; 
    public $onClose = null; 
    // 连接 
    public $socket = null; 
    public function __construct($socket_address){
		$this->socket=stream_socket_server($socket_address); 
        echo $socket_address."\n"; 
    }
    // ... // stream_select 
    public function on() {}
    // 需要处理事情 
    public function accept() { 
        // 接收连接和处理使用 
        while (true) { 
            $client = @stream_socket_accept($this->socket); 
			// is_callable判断一个参数是不是闭包 
			if (is_callable($this->onConnect)) { 
                // 执行函数 
                ($this->onConnect)($this, $client); 
            }
            // tcp 处理 大数据 重复多发几次 
            // $buffer = ""; 
            // while (!feof($client)) { // $buffer = $buffer.fread($client, 65535); // } 
            $data = fread($client, 65535); 
            if (is_callable($this->onReceive)) { 
               ($this->onReceive)($this, $client, $data); 
            }
            // 处理完成之后关闭连接 
           	fclose($client); 
		} 
	}
	// 发送信息 
    public function send($conn, $data) { 
        fwrite($conn, $data); 
    }
    // 启动服务的 
    public function start() {
        $this->accept();
    }
}
?>
```

```php
<?php
    // io\test\blocking\server.php
    require __DIR__.'/../../vendor/autoload.php';
    use ShineYork\Io\Blocking\Worker;
    $host = "tcp://0.0.0.0:9000";
    $server = new Worker($host);
    // $server->onConnect = function($socket, $client){
    // echo "有一个连接进来了\n";
    // var_dump($client);
    // };
    // 接收和处理信息
    $server->onReceive = function($socket, $client, $data){
        echo "给连接发送信息\n";
        $socket->send($client, "hello world client \n");
        // fwrite($client, "server hellow");
    };
    $server->start();
?>
```

### 0.9 认识非阻塞模型、多路复用与信号函数

#### 0.9.1 简单快速实现长链接

​		实现方式：通过在服务端不主动fclose连接，在客户端通过轮询请求的方式粗暴的实现

​		示例代码

```php
<?php
namespace ShineYork\Io\Blocking;
//服务器端
// 这是等会自个要写的服务
class Worker
{

    // 自定义服务的事件注册函数，
    // 这三个是闭包函数
    public $onReceive = null;
    public $onConnect = null;
    public $onClose = null;

    // 连接
    public $socket = null;

    public function __construct($socket_address)
    {
        $this->socket = stream_socket_server($socket_address);
        echo $socket_address."\n";
    }
    // ...
    // stream_select
    public function on()
    {
    }

    // 需要处理事情
    public function accept()
    {
        // 接收连接和处理使用
        while (true) {
            // 监听的过程是阻塞的
            $client = stream_socket_accept($this->socket);
            // is_callable判断一个参数是不是闭包
            if (is_callable($this->onConnect)) {
                // 执行函数
                ($this->onConnect)($this, $client);
            }
            // tcp 处理 大数据 重复多发几次
            // $buffer = "";
            // while (!feof($client)) {
            //    $buffer = $buffer.fread($client, 65535);
            // }
            $data = fread($client, 65535);
            if (is_callable($this->onReceive)) {
                ($this->onReceive)($this, $client, $data);
            }
            // 处理完成之后关闭连接
            //
            //
            // 心跳检测 - 自己的心跳
            // fclose($client);
        }
    }
    // 发送信息
    // public function send($conn, $data)
    // {
    //     fwrite($conn, $data);
    // }
    public function send($client, $data)
    {
        $response = "HTTP/1.1 200 OK\r\n";
        $response .= "Content-Type: text/html;charset=UTF-8\r\n";
        $response .= "Connection: keep-alive\r\n";
        $response .= "Content-length: ".strlen($data)."\r\n\r\n";
        $response .= $data;
        fwrite($client, $response);
    }

    // 启动服务的
    public function start()
    {
        $this->accept();
    }
}

```

```php
<?php
 //客户端
// 是建立连接
$client = stream_socket_client("tcp://127.0.0.1:9000");
$new = time();
// 给socket通写信息
// 粗暴的方式去实现
while (true) {
  fwrite($client, "hello world");
  var_dump(fread($client, 65535));
  sleep(2);
}
// 读取信息

// 关闭连接
// fclose($client);
```

#### 0.9.2 非阻塞模型

#### 0.9.2.1 实现非阻塞模型

​		这种实现比较简单核心就在于轮询不断的查看进程的状态，并不会阻塞与其他进程的操作，如果说当前进程模型没有处理完就会做其他事情；但是缺点就是会不断地询问内核，这将占用大量的CPU时间，系统资源利用 率较低，所以一般web服务器不适用这种I/O模型； 

​		首先我们可以保持之前阻塞模型的服务不改-》因为咋们这个功能主要是体现在于客户端对于服务端的信息获取；当然建议还是copy一份 ip\src\Blocking\Worker.php 代码放到 io\src\NonBlocking\Worker.php 以 做区分；当然也别忘了测试的代码 io\test\blocking\server.php copy到 io\test\non_blocking\server.php

```php

<?php
    // io\src\NonBlocking\Worker.php
    namespace ShineYork\Io\NonBlocking;
    // 这是等会自个要写的服务
    class Worker
    {
        // 自定义服务的事件注册函数，
        // 这三个是闭包函数
        public $onReceive = null;
        public $onConnect = null;
        public $onClose = null;
        // 连接
        public $socket = null;
        public function __construct($socket_address)
        {
            $this->socket = stream_socket_server($socket_address);
            // 
            echo $socket_address."\n";
        }
        // ...
        // stream_select
        public function on()
        {
        }

        // 需要处理事情
        public function accept()
        {
            // 接收连接和处理使用
            while (true) {
                // 监听的过程是阻塞的
                $client = stream_socket_accept($this->socket);
                // is_callable判断一个参数是不是闭包
                if (is_callable($this->onConnect)) {
                    // 执行函数
                    ($this->onConnect)($this, $client);
                }
                // tcp 处理 大数据 重复多发几次
                // $buffer = "";
                // while (!feof($client)) {
                //    $buffer = $buffer.fread($client, 65535);
                // }
                $data = fread($client, 65535);
                if (is_callable($this->onReceive)) {
                    ($this->onReceive)($this, $client, $data);
                }
                // 处理完成之后关闭连接
                //
                //
                // 心跳检测 - 自己的心跳
                fclose($client);
            }
        }
        // 发送信息
        // public function send($conn, $data)
        // {
        //     fwrite($conn, $data);
        // }
        public function send($client, $data)
        {
            $response = "HTTP/1.1 200 OK\r\n";
            $response .= "Content-Type: text/html;charset=UTF-8\r\n";
            $response .= "Connection: keep-alive\r\n";
            $response .= "Content-length: ".strlen($data)."\r\n\r\n";
            $response .= $data;
            fwrite($client, $response);
        }
        // 启动服务的
        public function start()
        {
            $this->accept();
        }
    }
?>
```

```php
<?php
require __DIR__.'/../../vendor/autoload.php';
use ShineYork\Io\NonBlocking\Worker;
$host = "tcp://0.0.0.0:9000";
$server = new Worker($host);
$server->onConnect = function($socket, $client){
    echo "有一个连接进来了\n";
    // var_dump($client);
};
// 接收和处理信息
$server->onReceive = function($socket, $client, $data){
    echo "给连接发送信息\n";
    sleep(3);
    $socket->send($client, "hello world client \n");
    // fwrite($client, "server hellow");
};
$server->start();
?>
```

##### 0.9.2.2 stream_select初体验

​		手册 https://php.golaravel.com/function.stream-select.html

​		作用：就是过滤出可读取的连接

```php
<?php
    while (!feof($client)) {
        // 接收的数据包的大小65535

        $read[] = $client;
        fread($client, 65535);
        // var_dump();
        // echo $r++."\n";
        sleep(1);
        echo "检查socket :\n";
        // 返回一个结果 0 可用 1，正忙
        var_dump(stream_select($read, $write, $except, 0));

        // foreach ($read as $value) {
        //   // code...
        // }
    }
?>
```

#### 0.9.3 多路复用

##### 0.9.3.1 代码实现

​		这种模式我们又可以称之为io多路复用 

​		服务监听流程如上 

1. 保存所有的socket,通过select系统调用，监听socket描述符的可读事件 

2. Select会在内核空间监听一旦发现socket可读，会从内核空间传递至用户空间，在用户空间通过逻辑判断是服务端socket可读，还是客户端的socket可读 

3. 如果是服务端的socket可读，说明有新的客户端建立，将socket保留到监听数组当中

4. 如果是客户端的socket可读，说明当前已经可以去读取客户端发送过来的内容了，读取内容，然后响应给客户端。 

​		缺点：

1. select模式本身的缺点（1. 循环遍历处理事件、2. 内核空间传递数据的消耗） 

2. 单进程对于大量任务处理乏力

```php
<?php
namespace ShineYork\Io\Multi;

// 这是等会自个要写的服务
class Worker
{

    // 自定义服务的事件注册函数，
    // 这三个是闭包函数
    public $onReceive = null;
    public $onConnect = null;
    public $onClose = null;

    protected $sockets = [];
    // 连接
    public $socket = null;

    public function __construct($socket_address)
    {
        $this->socket = stream_socket_server($socket_address);
        stream_set_blocking($this->socket, 0);
        // 咋们的server也有忙的时候
        $this->sockets[(int) $this->socket] = $this->socket;
        // echo $socket_address."\n";
    }

    // 需要处理事情
    public function accept()
    {
        // 接收连接和处理使用
        while (true) {

            $read = $this->sockets;
            // 校验池子是否有可用的连接 -》 校验传递的数组中是否有可以用的连接 socket
            // 把连接放到$read
            // 它返回值其实并不是特别可靠

            // $this->debug('这是stream_select 检测之 start 的 $read');
            // $this->debug($read, true);

            stream_select($read, $w, $e, 1);

            // $this->debug('这是stream_select 检测之 end 的 $read');
            // $this->debug($read, true);
            // sleep(1);
            foreach ($read as $socket) {
                // $socket 可能为
                if ($socket === $this->socket) {
                    // 创建与客户端的连接
                    $this->createSocket();
                } else {
                    // 发送信息
                    $this->sendMessage($socket);
                }
                // 1. 主worker
                // 2. 也可能是通过 stream_socket_accept 创建的连接
            }
            // // 监听的过程是阻塞的
            // $client = stream_socket_accept($this->socket);
            // // is_callable判断一个参数是不是闭包
            // if (is_callable($this->onConnect)) {
            //     // 执行函数
            //     ($this->onConnect)($this, $client);
            // }
            // // tcp 处理 大数据 重复多发几次
            // // $buffer = "";
            // // while (!feof($client)) {
            // //    $buffer = $buffer.fread($client, 65535);
            // // }
            // $data = fread($client, 65535);
            // if (is_callable($this->onReceive)) {
            //     ($this->onReceive)($this, $client, $data);
            // }
            // 处理完成之后关闭连接
            //
            //
            // 心跳检测 - 自己的心跳
            // fclose($client);
        }
    }

    public function createSocket()
    {
        $client = stream_socket_accept($this->socket);
        // is_callable判断一个参数是不是闭包
        if (is_callable($this->onConnect)) {
            // 执行函数
            // ($this->onConnect)($this, $client);
        }
        // 把创建的socket的连接 -》 放到 $this->sockets
        $this->sockets[(int) $client] = $client;
    }

    public function sendMessage($client)
    {
        $data = fread($client, 65535);
        if ($data === '' || $data == false) {
            // 关闭连接
            // fclose($client);
            // unset($this->sockets[(int) $client]);
            return null;
        }
        if (is_callable($this->onReceive)) {
            ($this->onReceive)($this, $client, $data);
        }
    }
    public function send($client, $data)
    {
        $response = "HTTP/1.1 200 OK\r\n";
        $response .= "Content-Type: text/html;charset=UTF-8\r\n";
        $response .= "Connection: keep-alive\r\n";
        $response .= "Content-length: ".strlen($data)."\r\n\r\n";
        $response .= $data;
        fwrite($client, $response);
    }

    public function debug($data, $flag = false)
    {
        if ($flag) {
            var_dump($data);
        } else {
            echo "==== >>>> : ".$data." \n";
        }
    }

    // 启动服务的
    public function start()
    {
        $this->accept();
    }
}
?>
```

```php
<?php
    // io\test\server.php
    require __DIR__.'/../vendor/autoload.php';
    use ShineYork\Io\Multi\Worker;
    $host = "tcp://0.0.0.0:9000";
    $server = new Worker($host);
    $server->onConnect = function($socket, $client){
        // echo "有一个连接进来了\n";
        // var_dump($client);
    };
    // 接收和处理信息
    $server->onReceive = function($socket, $client, $data){
    // echo "给连接发送信息\n";
    // sleep(3);
    $socket->send($client, "hello world client \n");
    	// fwrite($client, "server hellow");
    };
    echo $host."\n";
    $server->start();
?>
```

##### 0.9.3.2 出现的问题

​		测试环境：测试安装ab的方式测试 安装 yum -y install httpd-tools

###### 0.9.3.2.1 问题1

```tex
PHP Warning: stream socket_ accept(): accept failed: Too many open files in /www/wwwroot/swoole_ 191 1/io/src/Multi/Worker.php on line 84
socket too many open files
```

​		解决思路参考网址：这是因为系统的socket上限了参考这个网址 https://blog.csdn.net/fdipzone/article/details/34588803

###### 0.9.3.2.2 问题2

```tex
PHP Warning: stream socket accept(: accept failed: Too many open files in/www/wwwroot/swoole 1911/io/src/Multi/Worker.php on line 84
```

​		问题 ：连接socket 超标会出现那样的问题设置socket个数解决

​		解决思路：ulimit -n 202048

###### 0.9.3.2.3 问题3

```tex
Warning: stream select(: You MUST recompile PHP with a larger value ofFD SETSIZE.
lt is set to 1024, but you have descriptors numbered at least as high as 1945.--enable-fd-setsize=2048 is recommended, but you may want to set itto equal the maximum number of open files supported by your system,in order to avoid seeing this error again at a later date. in/www/wwwroot/swoole 1911/io/src/Multi/Worker.php on line 41
```

​		问题：超出了检查的上限个数

​		io多路复用：当我连接很多的时候stream select经历轮询处理 递归

#### 0.9.4 初识信号驱动函数

##### 0.9.4.1 信号函数介绍

​		pcntl_signa函数 : https://php.golaravel.com/function.pcntl-signa.html

​		posix_kill函数 : https://php.golaravel.com/function.posix-kill.html 

​		pcntl_signal_dispatch函数 : https://php.golaravel.com/function.pcntl-signa-dispatch.html

##### 0.9.4.2 实现代码

```php
<?php
    // 准备一个信号
    pcntl_signal(SIGIO,"sig_handler");
    function sig_handler($sig)
    {
    sleep(2);
    echo "one second after\n";
    }
    echo "1\n";
    // 安装信号 -》 给某某进程安装某某信号
    posix_kill(posix_getpid(),SIGIO);
    // 分发
    pcntl_signal_dispatch();
?>
```

![image-20230119112731743](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119112731743.png)

### 0.10 信号驱动与event事件

#### 0.10.1 长连接及多路复用的疑问

​		出现的问题：客户端和服务端都不在关闭fclose函数，发生了阻塞现象

​		客户端代码

![image-20230119152543876](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119152543876.png)

​		服务端相关代码

![image-20230119152608147](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119152608147.png)

​		采用io多路复用代码，可以解决阻塞现象，下图是

![image-20230119152742465](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119152742465.png)

​		多路复用的部分代码

![image-20230119152825190](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119152825190.png)

多路复用可能存在的现象：执行流程是串行的。而不是并行的

#### 0.10.2 信号模型

​		示例代码

```php
<?php
namespace ShineYork\Io\Signal;
// 这是等会自个要写的服务
class Worker {
    // 自定义服务的事件注册函数，
    // 这三个是闭包函数
    public $onReceive = null;
    public $onConnect = null;
    public $onClose = null;
    // 连接
    public $socket = null;
    public function __construct($socket_address){
    	$this->socket = stream_socket_server($socket_address);
    	echo $socket_address."\n";
    }
    // 需要处理事情
    public function accept(){
        // 接收连接和处理使用
        while (true) {
            $this->debug("accept start");
            // 监听的过程是阻塞的
            $client = stream_socket_accept($this->socket);
            pcntl_signal(SIGIO, $this->sigHander($client));
            posix_kill(posix_getpid(), SIGIO);
            // 分发
            pcntl_signal_dispatch();
            $this->debug("accept end");
            // 处理完成之后关闭连接
            // 心跳检测 - 自己的心跳
            // fclose($client);
        }
    }
    public function sigHander($client){
        return function($sig) use ($client){
            // is_callable判断一个参数是不是闭包
            if (is_callable($this->onConnect)) {
                // 执行函数
                ($this->onConnect)($this, $client);
            }
            $data = fread($client, 65535);
            if (is_callable($this->onReceive)) {
            	($this->onReceive)($this, $client, $data);
            }
        };
    }
    public function debug($data, $flag = false){
        if ($flag) {
        	var_dump($data);
        } else {
        	echo "==== >>>> : ".$data." \n";
        }
    }
    // 发送信息
    public function send($client, $data){
        $response = "HTTP/1.1 200 OK\r\n";
        $response .= "Content-Type: text/html;charset=UTF-8\r\n";
        $response .= "Connection: keep-alive\r\n";
        $response .= "Content-length: ".strlen($data)."\r\n\r\n";
        $response .= $data;
        fwrite($client, $response);
    }
    // 启动服务的
    public function start(){
    	$this->accept();
    }
}
?>
```

​		缺点 : 信号I/O在大量IO操作时可能会因为信号队列溢出导致没法通知信号驱动I/O尽管对于处理UDP套接字来说有用，即这种信号通知意味着到达一个数据报，或者返回一个异步错误。但是，对于TCP而言，信号驱动的I/O方 式近乎无用，因为导致这种通知的条件为数众多，每一个来进行判别会消耗很大资源，与前几种方式相比优势尽失

#### 0.10.3 event事件

##### 0.10.3.1 介绍与安装

​		由POSIX规范定义，应用程序告知内核启动某个操作，并让内核在整个操作（包括将数据从内核拷贝到应用程序的缓冲区）完成后通知应用程序。这种模型与信号驱动模型的主要区别在于：信号驱动I/O是由内核 通知应用程序何时启动一个I/O操作，而异步I/O模型是由内核通知应用程序I/O操作何时完成 

​		在学习之前我们需要使用到PHP中的一个扩展函数Event 系列-> 事件; 手册地址 https://php.golaravel.com/class.event.html 

​		第一步先安装：event 注意当前我们 

​		首先需要安装一下linux系统的libevent，然后再安装php7,-event扩展，素材均以准备好；

```tex
$ tar -zxvf libevent-2.1.8-stable.tar.gz
$ cd libevent-2.1.8-stable
$ ./configure --prefix=/usr/local/libevent-2.1.8
$ make
$ make install
$ ...
$ tar -zxvf event-2.3.0.tgz
$ cd event-2.3.0
$ phpize
$ ./configure --with-php-config=/www/server/php/73/bin/php-config
$ make
$ make install
$ php -m | grep event

```

##### 0.10.3.2 event初体验

​		来个初体验把，这个函数呢 -》 其实是类似于laravel框架中的event；

​		也就是事件操作区别就在于event是一个原生的依赖于libevent而实现的；不过这个函数的使用，资源很稀有； 这个函数就是PHP中的事件函数，而事件标签可以看手册了解 https://php.golaravel.com/event.flags.html 

​		体验一下 : https://segmentfault.com/a/1190000016254243; 根据这个网址的案例及可以体验好

```php
<?php
    $eventBase = new EventBase();
    // 初始化一个定时器event（歼15，然后放到辽宁舰机库中）
    $timer = new Event( $eventBase, -1, Event::TIMEOUT | Event::PERSIST, function(){
    echo microtime( true )." : 歼15，滑跃，起飞！".PHP_EOL;
    });
    // tick间隔为0.05秒钟，我们还可以改成0.5秒钟甚至0.001秒，也就是毫秒级定时器
    $tick = 0.05;
    // 将定时器event添加（将歼15拖到甲板加上弹射器）
    $timer->add( $tick );
    // eventBase进入loop状态（辽宁舰！走你！）
    $eventBase->loop();
?>
```

##### 0.10.3.3 event与socket操作演示

示例代码

```php
<?php
    // socket.php
    $socket = stream_socket_server("tcp://0.0.0.0:9000", $errno, $errstr);
    stream_set_blocking($socket, 0);
    $eventBase = new EventBase;
    $event = new Event($eventBase, $socket, Event::READ | Event::PERSIST, function($socket) use ($eventBase) {
        echo "连接 start \n";
        $conn = stream_socket_accept($socket);
        stream_set_blocking($conn, false);
        var_dump(fread($conn, 65535));
        fwrite($conn, "hello event");
        echo "连接 end \n";
    });
    $event->add();
    $eventBase->loop();
?>

```

​		运行 `php socket.php` 既可以看到效果，不过这个event存在嵌套定义事件 的问题；比如现在需要针对于socket再定义一个write的事件；那么这个时候最好的办法就是代码如下的方式调整；

```php
<?php
    // server.php
    $socket = stream_socket_server("tcp://0.0.0.0:9000", $errno, $errstr);
    stream_set_blocking($socket, 0);
    $eventBase = new EventBase;
    $event = new Event($eventBase, $socket, Event::READ | Event::PERSIST, function($socket) use ($eventBase) {
        echo "连接 start \n";
        $conn = stream_socket_accept($socket);
        stream_set_blocking($conn, false);
        $event = new Event($eventBase, $conn, Event::WRITE | Event::PERSIST, function($conn) use ($eventBase) {
            echo "WRITE start \n";
            var_dump(fread($conn, 65535));
            fwrite($conn, "hello event");
            echo "WRITE end \n";
        });
        $event->add();
        echo "连接 end \n";
    });
    $event->add();
    $eventBase->loop();
?>
```

​		可以看到效果是不行的；这个和event这个对象有关系，因为它在同作用域下在重新定义不生效避免覆盖原有的event；因此我们需要做一个额外操作；可以创建另一个文件 比如E.php,还有为了与生效我们还需要额外的

##### 0.10.3.4 event解释

​		在server.php中定义一个额外的参数作为记录，属性名不要管event类需要

```php
<?php
use \Event as Event;
class e {
    protected $client;
    protected $eventBase;
    function __construct($eventBase, $client, &$count){
        $this->eventBase = $eventBase;
        $this->client = $client;
    }
    public function handler(){
        $event = new Event($this->eventBase, $this->client, Event::PERSIST |Event::READ | Event::WRITE , function($socket){
            // 对于建立处理时间
            var_dump(fread($socket, 65535));
            fwrite($socket, " 提前祝大家平安夜快乐 \n");
            fclose($socket);
            ($this->count[(int) $socket][Event::PERSIST | Event::READ | Event::WRITE])->free();
        });
        $event->add();
        $this->count[(int) $this->client][Event::PERSIST | Event::READ | Event::WRITE] = $event;
        var_dump($this->count);
    }
}
?>
```

```php
<?php
    require 'e.php';
    use \Event as Event;
    use \EventBase as EventBase;
    $socket_address = "tcp://0.0.0.0:9000";
    $server = stream_socket_server($socket_address);
    echo $socket_address."\n";
    $eventBase = new EventBase();
    // 记录我们所创建的这样事件 让 $eventBase 可以找到这个事件
    $count = [];// 变量没有要求 随便
    $event = new Event($eventBase, $server, Event::PERSIST | Event::READ | Event::WRITE , function($socket) use ($eventBase, &$count){
        // 在闭包中的 function($socket) 的$socket 就是
        // 在构造函数中传递的 $server 这个属性
        // 也就是 $socket = $server
        // 建立与用户的连接
        echo "连接 start \n";
        $client = stream_socket_accept($socket);
        (new E($eventBase, $client, $count))->handler();
        echo "连接 end \n";
    });
    $event->add();
    $count[(int) $server][Event::PERSIST | Event::READ | Event::WRITE] = $event;
    var_dump($count);
    $eventBase->loop();
?>
```

### 0.11 swoole复习之网络模型与stream相关函数

#### 0.11.1 学习swoole需要准备的基础知识与它能做啥

![image-20230119175309704](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119175309704.png)





#### 0.11.2 网络通信之tcp与upd

##### 0.11.2.1 回顾网络通信

![image-20230119175321885](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119175321885.png)

![image-20230119175336869](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119175336869.png)

##### 0.11.2.2 tcp

###### 0.11.2.2.1 三次握手四次挥手

​		请参考[TPC的三次握手四次挥手](#### 0.4.2.2)

##### 0.11.2.3 upd

​		请参考[TCP与UDP区别](#### 0.4.2.1)

#### 0.11.3 网络io模型

![image-20230119180658560](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119180658560.png)

#### 0.11.4 stream系列函数介绍

![image-20230119180719547](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119180719547.png)

![image-20230119180736433](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119180736433.png)

### 0.12 swoole复习之回顾网络IO与event事件

#### 0.12.1 追加助手函数

​		为了与方便操作，我们定义一个该组件的助手函数方式，只需要在composer.json中增加一项操作即可

```json
{
    "name": "shineyork/io",
    "description": "io",
    "type": "library",
    "license": "MIT",
    "authors": [
        {
            "name": "shineyork",
            "email": "shineyork@sixstaredu.com"
        }
    ],
    "autoload":{
        "psr-4":{
        	"ShineYork\\Io\\" : "./src/"
        },
        "files":[
        	"src/Helper.php"
        ]
    },
    "require": {}
}
```

​		再执行 `composer update` 即可

```php
<?php
    // src/Helper.php
    function debug($data, $flag = false){
        if ($flag) {
        	var_dump($data);
        } else {
        	echo "==== >>>> : ".$data." \n";
        }
    }
    // 发送信息
    function send($client, $data, $flag = true){
        if ($flag) {
        	fwrite($client, $data);
        } else {
            $response = "HTTP/1.1 200 OK\r\n";
            $response .= "Content-Type: text/html;charset=UTF-8\r\n";
            $response .= "Connection: keep-alive\r\n";
            $response .= "Content-length: ".strlen($data)."\r\n\r\n";
            $response .= $data;
            fwrite($client, $response);
        }
    }
?>

```

#### 0.12.2 event事件

##### 0.12.2.1 安装

​		由POSIX规范定义，应用程序告知内核启动某个操作，并让内核在整个操作（包括将数据从内核拷贝到应用程序的缓冲区）完成后通知应用程序。**这种模型与信号驱动模型的主要区别在于：信号驱动I/O是由内核 通知应用程序何时启动一个I/O操作，而异步I/O模型是由内核通知应用程序I/O操作何时完成 **

​		在学习之前我们需要使用到PHP中的一个扩展函数Event 系列-> 事件; 手册地址 https://php.golaravel.com/class.event.html

​		第一步先安装：event 注意当前我们 

​		首先需要安装一下linux系统的libevent，然后再安装php7,-event扩展，素材均以准备好；

```tex
$ tar -zxvf libevent-2.1.8-stable.tar.gz
$ cd libevent-2.1.8-stable
$ ./configure --prefix=/usr/local/libevent-2.1.8
$ make
$ make install
$ ...
$ tar -zxvf event-2.3.0.tgz
$ cd event-2.3.0
$ phpize
$ ./configure --with-php-config=/www/server/php/73/bin/php-config
$ make
$ make install
$ php -m | grep event
```

##### 0.12.2.2 使用event及坑

​		来个初体验把，这个函数呢 -》 其实是类似于laravel框架中的event；也就是事件操作区别就在于event是一个原生的依赖于libevent而实现的；不过这个函数的使用，资源很稀有； 

​		这个函数就是PHP中的事件函数，而事件标签可以看手册了解 https://php.golaravel.com/event.flags.html 

​		体验一下 : https://segmentfault.com/a/1190000016254243; 根据这个网址的案例及可以体验好

```php
<?php
    $eventBase = new EventBase();
    // 初始化一个定时器event（歼15，然后放到辽宁舰机库中）
    $timer = new Event( $eventBase, -1, Event::TIMEOUT | Event::PERSIST, function(){
    	echo microtime( true )." : 歼15，滑跃，起飞！".PHP_EOL;
    });
    // tick间隔为0.05秒钟，我们还可以改成0.5秒钟甚至0.001秒，也就是毫秒级定时器
    $tick = 0.05;
    // 将定时器event添加（将歼15拖到甲板加上弹射器）
    $timer->add( $tick );
    // eventBase进入loop状态（辽宁舰！走你！）
    $eventBase->loop();
?>
```

​		接下来我们让event与socket结合；

```php
// socket.php
<?php
    $socket = stream_socket_server("tcp://0.0.0.0:9000", $errno, $errstr);
    stream_set_blocking($socket, 0);
    $eventBase = new EventBase;
    $event = new Event($eventBase, $socket, Event::READ | Event::PERSIST, function($socket) use ($eventBase) {
        echo "连接 start \n";
        $conn = stream_socket_accept($socket);
        stream_set_blocking($conn, false);
        var_dump(fread($conn, 65535));
        fwrite($conn, "hello event");
        echo "连接 end \n";
    });
    $event->add();
    $eventBase->loop();
?>
```

​		运行 `php socket.php` 既可以看到效果，不过这个event存在嵌套定义事件 的问题；比如现在需要针对于socket再定义一个write的事件；那么这个时候最好的办法就是代码如下的方式调整；

```php
<?php
    // server.php
    $socket = stream_socket_server("tcp://0.0.0.0:9000", $errno, $errstr);
    stream_set_blocking($socket, 0);
    $eventBase = new EventBase;
    $event = new Event($eventBase, $socket, Event::READ | Event::PERSIST, function($socket) use ($eventBase) {
        echo "连接 start \n";
        $conn = stream_socket_accept($socket);
        stream_set_blocking($conn, false);
        $event = new Event($eventBase, $conn, Event::WRITE | Event::PERSIST, function($conn) use ($eventBase) {
            echo "WRITE start \n";
            var_dump(fread($conn, 65535));
            fwrite($conn, "hello event");
            echo "WRITE end \n";
    	});
    	$event->add();
    	echo "连接 end \n";
    });
    $event->add();
    $eventBase->loop();
?>
```

![image-20230119231146208](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119231146208.png)

​		可以看到效果是不行的；这个和event这个对象有关系，因为它在同作用域下在重新定义不生效避免覆盖原有的event；因此我们需要做一个额外操作；可以创建另一个文件 比如E.php,还有为了与生效我们还需要额外的 在server.php中定义一个额外的参数作为记录，属性名不要管event类需要

```php
e.php
<?php
use \Event as Event;
class e {
    protected $client;
    protected $eventBase;
    function __construct($eventBase, $client, &$count){
        $this->eventBase = $eventBase;
        $this->client = $client;
    }
    public function handler(){
        $event = new Event($this->eventBase, $this->client, Event::PERSIST |Event::READ | Event::WRITE , function($socket){
            // 对于建立处理时间
            var_dump(fread($socket, 65535));
            fwrite($socket, " 提前祝大家平安夜快乐 \n");
            fclose($socket);
            ($this->count[(int) $socket][Event::PERSIST | Event::READ | Event::WRITE])->free();
		});
        $event->add();
        $this->count[(int) $this->client][Event::PERSIST | Event::READ | Event::WRITE] = $event;
        var_dump($this->count);
    }
}
?>
```

```php
eventSocket.php
<?php
    require 'e.php';
    use \Event as Event;
    use \EventBase as EventBase;
    $socket_address = "tcp://0.0.0.0:9000";
    $server = stream_socket_server($socket_address);
    echo $socket_address."\n";
    $eventBase = new EventBase();
    // 记录我们所创建的这样事件 让 $eventBase 可以找到这个事件
    $count = [];// 变量没有要求 随便
    $event = new Event($eventBase, $server, Event::PERSIST | Event::READ | Event::WRITE , function($socket) use ($eventBase, &$count){
        // 在闭包中的 function($socket) 的$socket 就是
        // 在构造函数中传递的 $server 这个属性
        // 也就是 $socket = $server
        // 建立与用户的连接
        echo "连接 start \n";
        $client = stream_socket_accept($socket);
        (new E($eventBase, $client, $count))->handler();
        echo "连接 end \n";
    });
    $event->add();
    $count[(int) $server][Event::PERSIST | Event::READ | Event::WRITE] = $event;
    var_dump($count);
    $eventBase->loop();
?>
```

### 0.13 异步IO的实现

 #### 0.13.1 swoole实现异步io模型

​		因为考虑到使用原生的event的方式相对来说比较麻烦所以还是使用swoole的方式 

​		在swoole中是提供一个event的操作函数的，https://wiki.swoole.com/wiki/page/p-event.html 

​		接下来我们使用 swoole\event 对象操作

 ```php
 <?php
 namespace ShineYork\Io\AsyncModel;
 
 use Swoole\Event;
 // 这是等会自个要写的服务
 class Worker
 {
 
     // 自定义服务的事件注册函数，
     // 这三个是闭包函数
     public $onReceive = null;
     public $onConnect = null;
     public $onClose = null;
 
     // 连接
     public $socket = null;
 
     public function __construct($socket_address)
     {
         $this->socket = stream_socket_server($socket_address);
     }
     // 需要处理事情
     public function accept()
     {
         // debug('accept start');
         Event::add($this->socket, $this->createSocket());
         // debug('accept end');
     }
 
     public function createSocket()
     {
         return function($socket){
             // $client 是不是资源 socket
             $client = stream_socket_accept($this->socket);
             // is_callable判断一个参数是不是闭包
             if (is_callable($this->onConnect)) {
                 // 执行函数
                 ($this->onConnect)($this, $client);
             }
             // 默认就是循环操作
             Event::add($client, $this->sendClient());
         };
     }
 
     public function sendClient()
     {
         return function($socket){
             //从连接当中读取客户端的内容
             $buffer=fread($socket,1024);
             //如果数据为空，或者为false,不是资源类型
             if(empty($buffer)){
                 if(feof($socket) || !is_resource($socket)){
                     //触发关闭事件
                     swoole_event_del($socket);
                     fclose($socket);
                 }
             }
             //正常读取到数据,触发消息接收事件,响应内容
             if(!empty($buffer) && is_callable($this->onReceive)){
                 ($this->onReceive)($this, $socket, $buffer);
             }
         };
     }
     //响应http请求
     public function send($conn, $content){
         $http_resonse = "HTTP/1.1 200 OK\r\n";
         $http_resonse .= "Content-Type: text/html;charset=UTF-8\r\n";
         $http_resonse .= "Connection: keep-alive\r\n";
         $http_resonse .= "Server: php socket server\r\n";
         $http_resonse .= "Content-length: ".strlen($content)."\r\n\r\n";
         $http_resonse .= $content;
         fwrite($conn, $http_resonse);
     }
     // 启动服务的
     public function start()
     {
         $this->accept();
     }
 }
 ?>
 ```

```php
<?php
    // io\test\event\server.php	
    require __DIR__.'/../../vendor/autoload.php';
    use ShineYork\Io\AsyncModel\Worker;
    $host = "tcp://0.0.0.0:9000";
    $server = new Worker($host);
    // echo 1;
    $server->onReceive = function($socket, $client, $data){
        // debug($data);
        // sleep(3);
        // echo "给连接发送信息\n";
        // 封装在src/Helper.php
        // $socket->send($client, "hello world client \n");
        send($client, "hello world client \n");
    };
    debug($host);
    $server->start();
?>
```

​		异常处理`error : too many open files`这个问题主要是因为与socket的上限问题 https://blog.csdn.net/fdipzone/article/details/34588803

​		可进行压力测试 `ab -n 10000 -c 10000 -k http://127.0.0.1:9000/ `

​		在课堂上 ab测试的时候有问题 ，原因是协议不对，修改的方式只需要把 `io\test\event\server.php` 修改一下send方法修改为 `send($client, "hello world client \n", false);`

#### 0.13.2 Reactor理解

##### 0.13.2.1 传统的方式

![image-20230119232058123](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119232058123.png)

​		特点：采用阻塞式I/O模型获取输入数据；每个连接都需要独立的线程完成数据输入，业务处理，数据返回的完整操作 

​		存在问题：当并发数较大时，需要创建大量线程来处理连接，系统资源占用较大；连接建立后，如果当前线程暂时没有数据可读，则线程就阻塞在read操作上，造成线程资源浪费

##### 0.13.2.2 Reactor模式

![image-20230119232241522](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119232241522.png)

​		Reactor模式，是指通过一个或多个输入同时传递给服务处理器的服务请求的事件驱动处理模式。 服务端程序处理传入多路请求，并将它们同步分派给请求对应的处理线程，Reactor模式也叫Dispatcher模式，即I/O多了 复用统一监听事件，收到事件后分发(Dispatch给某进程)，是编写高性能网络服务器的必备技术之一 

​		Reactor模式中有2个关键组成： 

* Reactor在一个单独的线程中运行，负责监听和分发事件，分发给适当的处理程序来对IO事件做出反应。 它就像公司的电话接线员，它接听来自客户的电话并将线路转移到适当的联系人 

* Handlers处理程序执行I/O事件要完成的实际事件，类似于客户想要与之交谈的公司中的实际官员。Reactor通过调度适当的处理程序来响应I/O事件，处理程序执行非阻塞操作

#### 0.13.3 单Reactor单线程

![image-20230119232422536](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230119232422536.png)

##### 0.13.3.1 方案说明

* Reactor对象通过select监控客户端请求事件，收到事件后通过dispatch进行分发 

* 如果是建立连接请求事件，则由Acceptor通过accept处理连接请求，然后创建一个Handler对象处理连接完成后的后续业务处理 

* 如果不是建立连接事件，则Reactor会分发调用连接对应的Handler来响应 

* Handler会完成read->业务处理->send的完整业务流程

##### 0.13.3.2 优点

​		模型简单，没有多线程、进程通信、竞争的问题，全部都在一个线程中完成

##### 0.13.3.3 缺点

* 性能问题：只有一个线程，无法完全发挥多核CPU的性能Handler在处理某个连接上的业务时，整个进程无法处理其他连接事件，很容易导致性能瓶颈 
* 可靠性问题：线程意外跑飞，或者进入死循环，会导致整个系统通信模块不可用，不能接收和处理外部消息，造成节点故障

##### 0.13.3.4 使用场景

​		客户端的数量有限，业务处理非常快速，比如Redis，业务处理的时间复杂度O(1) 

​		而我们之前的模型则就是单Reactor单线程这样的模型; 注意在我们的代码中因为没有直接去调用dispatch方法，因为我们使用的是swoole中的event在进行add的时候已经处理了：这里分别把swoole的方式及使用PHP中 event扩展的方式实现的单Reactor给大家

## 1 基础篇

### 1.1 Reactor模型

#### 1.1.1  多进程

​		fork函数在调用一次之后就会返回两次，他可能有三种不同的返回值： 

1. 在父进程中，fork返回新创建子进程的进程id

2. 在子进程中，fork返回0

3. 如果出现错误，fork返回一个负值 

​		在fork函数执行完毕后，如果创建新进程成功，则出现两个进程，一个是子进程，- -个是父进程。在子进程中，fork函 数返回0，在父进程中，fork返回新创建子进程的进程ID。我们可以通过fork返回的值来判断当前进 程是子进程还是父进程。 

​		引用一-位网友的话来解释fpid的值为什么在父子进程中不同。“其实就相当于链表，进程形成了链表，父进程的fpid(p意味point)指向子进程.的进程id,因为子进程没有子进程，所以其fpid为0. 

​		fork出错可能有两种原因: 

1. 当前的进程数已经达到了系统规定的上限，这时errno的值被设置为EAGAIN。

2. 系统内存不足，这时errno的值被设置为ENOMEM。  

​		创建新进程成功后，系统中出现两个基本完全相同的进程，这两个进程执行没有固定的先后顺序，哪个进程先执行要看系统的进程调度策略。每个进程都有一个独特(互不相同)的进程标识符(process ID)，可以通过 getpid()函数获得，还有一个记录父进程pid的变量，可以通过getppid()函数获得变量的值。

##### 1.1.1.1 php创建子进程演示 

​		在PHP中关于创建进程的方法主要是通过与pcntl_fork()函数创建的， 可以通过posix_getpid() 获取到进程的id 如下示例： 

​		额外需要注意的是pcntl是需要在linux下安装的

```php
[root@localhost 05]# php -m | grep pcnt
pcntl
```

​		php示例代码：

```php
<?php
    $curr_pid = posix_getpid();//获取当前的进程id
    // 将当前进程的id写入文件中
    echo '当前父进程的id：'.$curr_pid."\n";
    // pcntl_fork 创建子进程
    // 返回子进程的id
    $son_pid = pcntl_fork();
    echo '创建的子进程的id：'.$son_pid."\n";
    echo '创建子进程之后当前的进程为：'.posix_getpid()."\n";
    echo "1\n";
?>
```

​		效果

```php
[root@localhost 05]# php pnctl.php 
当前父进程的id：8742 
创建的子进程的id：8743 
创建子进程之后当前的进程为：8742
1
创建的子进程的id：0
创建子进程之后当前的进程为：8743
1
```

#### 1.2 预派生子进程模型

##### 1.2.1 理解

​		程序启动后就会创建N个进程。每个子进程进入Accept，等待新的连接进入。当客户端连接到服务器时，其中一个子进程会被唤醒，开始处理客户端请求，并且不再接受新的TCP连接。当此连接关闭时，子进程会释 放，重新进入Accept，参与处理新的连接。这个模型的优势是完全可以复用进程，不需要太多的上下文切换，比如php-fpm基于此模型的。

```php
<?php
namespace ShineYork\Io\MultithreadingBlocking;
class Worker{
    //监听socket
    protected $socket = NULL;
    //连接事件回调
    public $onConnect = NULL;
    //接收消息事件回调
    public $onMessage = NULL;
    // public $workerNum = 4;
    protected $config = [
    'workerNum' => 4,
    ];
    public function __construct($socket_address) {
    	//监听地址+端口
    	$this->socket=stream_socket_server($socket_address);
    }
    public function set($data)
    {
    // ...
    // 简单点
    $this->config = $data;
    }
    public function start()
    {
    $this->fork();
    }
    // 创建进程完成事情
    public function fork() {
        for ($i=0; $i < $this->config['workerNum']; $i++) {
            // 创建子进程
            $son_pid = pcntl_fork();
            if ($son_pid > 0) {
                // 父进程空间
            } else if ($son_pid < 0){
                $this->send($this->socket, "服务器异常");
            } else {
            echo $son_pid."\n";
                // 由子进程完成事情
                $this->accept();
            }
    	}
        // 父进程监听子进程情况并回收进程
        if ($son_pid) {
            $status = 0;
            $sop = \pcntl_wait($status);
            echo $sop."\n";
          }
    }
    public function accept() {
        while (true){ //循环监听
            $client = stream_socket_accept($this->socket);//在服务端阻塞监听
            if(!empty($client) && is_callable($this->onConnect)){//socket连接成功并且是我们的回调
            	//触发事件的连接的回调
            	call_user_func($this->onConnect, $client);
            }
            //从连接中读取客户端内容
            $buffer=fread($client,65535);//参数2：在缓冲区当中读取的最大字节数
            //正常读取到数据。触发消息接收事件，进行响应
            if(!empty($buffer) && is_callable($this->onMessage)){
            	//触发时间的消息接收事件
            	//传递到接收消息事件》当前连接、接收到的消息
            	call_user_func($this->onMessage,$this,$client,$buffer);
            }
            \fclose($client);
        }
    }
    //响应http请求
    public function send($conn,$content){
        $http_resonse = "HTTP/1.1 200 OK\r\n";
        $http_resonse .= "Content-Type: text/html;charset=UTF-8\r\n";
        $http_resonse .= "Connection: keep-alive\r\n";
        $http_resonse .= "Server: php socket server\r\n";
        $http_resonse .= "Content-length: ".strlen($content)."\r\n\r\n";
        $http_resonse .= $content;
        fwrite($conn, $http_resonse);
    }
}
?>
```

#### 1.3 单Reactor多线程/进程

![image-20230120182445182](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230120182445182.png)

![image-20230120182451902](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230120182451902.png)

##### 1.3.1 方案说明

* Reactor对象通过select监控客户端请求事件，收到事件后通过dispatch进行分发 

* 如果是建立连接请求事件，则由Acceptor通过accept处理连接请求，然后创建一个Handler对象处理连接完成后的续各种事件 

* 如果不是建立连接事件，则Reactor会分发调用连接对应的Handler来响应 

* Handler只负责响应事件，不做具体业务处理，通过read读取数据后，会分发给后面的Worker线程池进行业务处理 

* Worker线程池会分配独立的线程完成真正的业务处理，如何将响应结果发给Handler进行处理 

* Handler收到响应结果后通过send将响应结果返回给client

##### 1.3.2 优点

​		可以充分利用多核CPU的处理能力 

​		stream_context_set_option: https://php.golaravel.com/function.stream-context-set-option.html 

​		在workerman中对于短空的重复监听的方式就是通过与设置stream_context_set_option参数，这个参数的作用就是可以设置短空可以复用监听；开启监听端口复用后允许多个无亲缘关系的进程监听相同的端口不过如果我 们是直接直接设置的话会出现 惊群效应 ，不过并无特别大的影响 

​		如下是workerman中的代码示例

![image-20230120182643448](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230120182643448.png)

​		惊群效应：简单点来说，就是一个一堆宅男群中，突然出现一个妹子的声音-》找某一个人

```php
<?php
namespace ShineYork\Io\Reactor\Swoole\Multithreading;
use Swoole\Event;
class WorkerEC {
    //监听socket
    protected $sockets = NULL;
    //连接事件回调
    public $onConnect = NULL;
    //接收消息事件回调
    public $onMessage = NULL;
    protected $config = [
    	'workerNum' => 4,
    ];
    protected $socket_address;
    public function __construct($socket_address) {
        //监听地址+端口
        // $this->socket = stream_socket_server($socket_address);
        $this->socket_address = $socket_address;
    }
    public function set($data){
        // 简单点
        $this->config = $data;
    }
    public function start(){
    	$this->fork();
    }
    /**
    * 创建进程完成事情
    * 六星教育 @shineyork老师
    * @return [type] [description]
    */
    public function fork() {
        $son_pid = pcntl_fork();
        for ($i=0; $i < $this->config['workerNum']; $i++) {
            if ($son_pid > 0) {
            	$son_pid = pcntl_fork();
            } else if($son_pid < 0){
            // 异常
            } else {
            	$this->accept();
            // exit;
            }
        }
        // 父进程监听子进程情况并回收进程
        for ($i=0; $i < $this->config['workerNum']; $i++) {
            $status = 0;
            $sop = pcntl_wait($status);
        }
    }
    // 接收连接，并处理连接
    public function accept(){
        // $this->sockets[(int) $socket] = $socket;
        // 第一个需要监听的事件(服务端socket的事件),一旦监听到可读事件之后会触发
        Event::add($this->initServer(), $this->createSocket());
    }
    /**
    * 初始化话server
    * 六星教育 @shineyork老师
    * @return [type] [description]
    */
    public function initServer() {
        $opts = [
            'socket' => [
            // 连接成功之后的等待个数
            'backlog' => '102400',
            ]
        ];
        $context = stream_context_create($opts);
        // 设置端口可以被多个进程重复的监听
        stream_context_set_option($context, 'socket', 'so_reuseport', 1);
        return stream_socket_server($this->socket_address, $errno, $errstr, STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, $context);
    }
    /**
    * 建立与客户端的连接
    * 六星教育 @shineyork老师
    * @return [type] [description]
    */
    public function createSocket(){
    return function($socket){
    // 测试端口监听的效果
    // $this->debug(posix_getpid());
    $client=stream_socket_accept($socket);
    //触发事件的连接的回调
    if(!empty($client) && is_callable($this->onConnect)){
    call_user_func($this->onConnect, $client);
    }
    Event::add($client, $this->sendMessage());
    };
    }
    /**
    * 发送信息
    * 六星教育 @shineyork老师
    * @return [type] [description]
    */
    public function sendMessage(){
        return function($socket){
            //从连接当中读取客户端的内容
            $buffer=fread($socket,1024);
            //如果数据为空，或者为false,不是资源类型
            if(empty($buffer)){
                if(feof($socket) || !is_resource($socket)){
                // 触发关闭事件
                // swoole_event_del($socket);
                // fclose($socket);
                }
            }
            //正常读取到数据,触发消息接收事件,响应内容
            if(!empty($buffer) && is_callable($this->onMessage)){
                call_user_func($this->onMessage,$this,$socket,$buffer);
            }
        };
	}
	public function debug($data, $flag = false){
        if ($flag) {
        \var_dump($data);
        } else {
        echo "==== >>>> : ".$data." \n";
        }
    }
    /**
    * 响应http请求
    * 六星教育 @shineyork老师
    * @param [type] $conn [description]
    * @param [type] $content [description]
    * @return [type] [description]
    */
    public function send($conn, $content){
        $http_resonse = "HTTP/1.1 200 OK\r\n";
        $http_resonse .= "Content-Type: text/html;charset=UTF-8\r\n";
        $http_resonse .= "Connection: keep-alive\r\n";
        $http_resonse .= "Server: php socket server\r\n";
        $http_resonse .= "Content-length: ".strlen($content)."\r\n\r\n";
        $http_resonse .= $content;
        fwrite($conn, $http_resonse);
    }
}
?>
```

​		当然我们可以进行压测 `ab -n 100000 -c 1000 -k http://127.0.0.1:9000/`不过有一点就是需要注意，我们系统处理请求并不是说我们多加几个进程效果就一定很好，其实这个还是需要取决于电脑的CPU的速度

#### 1.4 热加载

```tex
[root@localhost test]# kill -l
    1) SIGHUP 2) SIGINT 3) SIGQUIT 4) SIGILL 5) SIGTRAP
    6) SIGABRT 7) SIGBUS 8) SIGFPE 9) SIGKILL 10) SIGUSR1
    11) SIGSEGV 12) SIGUSR2 13) SIGPIPE 14) SIGALRM 15) SIGTERM
    16) SIGSTKFLT 17) SIGCHLD 18) SIGCONT 19) SIGSTOP 20) SIGTSTP
    21) SIGTTIN 22) SIGTTOU 23) SIGURG 24) SIGXCPU 25) SIGXFSZ
    26) SIGVTALRM 27) SIGPROF 28) SIGWINCH 29) SIGIO 30) SIGPWR
    31) SIGSYS 34) SIGRTMIN 35) SIGRTMIN+1 36) SIGRTMIN+2 37) SIGRTMIN+3
    38) SIGRTMIN+4 39) SIGRTMIN+5 40) SIGRTMIN+6 41) SIGRTMIN+7 42) SIGRTMIN+8
    43) SIGRTMIN+9 44) SIGRTMIN+10 45) SIGRTMIN+11 46) SIGRTMIN+12 47) SIGRTMIN+13
    48) SIGRTMIN+14 49) SIGRTMIN+15 50) SIGRTMAX-14 51) SIGRTMAX-13 52) SIGRTMAX-12
    53) SIGRTMAX-11 54) SIGRTMAX-10 55) SIGRTMAX-9 56) SIGRTMAX-8 57) SIGRTMAX-7
    58) SIGRTMAX-6 59) SIGRTMAX-5 60) SIGRTMAX-4 61) SIGRTMAX-3 62) SIGRTMAX-2
    63)SIGRTMAX-1 64)SIGRTMAX
```

![image-20230120183139781](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230120183139781.png)

​		演示代码

```php
<?php
namespace ShineYork\Io;

/**
 *
 */
class Index
{
    public function index()
    {
        echo "测 shineyork pppopop ff 试ff\n";
        debug('oo  oo');
    }
}
?>
```

`netstat -apn | grep 端口`

操作解释

* 先运行代码，然后访问地址

* 找 到swoole的进程，通过执行 k i l l -10 端口的方式发送一个信号给swoole 

* swoole 重 新 加 载 worker 

* 然后再次访问内容，可 以 看 到 代 码 加 载 了

#### 1.5 实现模型的reload功能

1. 记录创建的子进程的pid，因为pid会随机； 
2. 然后再创建一个reload方法 
3. 在方法中获取记录的pid，然后循环停止pid进程
4. 再重新启动进程

```php
<?php
namespace ShineYork\Io\Reactor\Swoole\Mulit;

use Swoole\Event;
// 这是等会自个要写的服务
class Worker
{
    // 自定义服务的事件注册函数，
    // 这三个是闭包函数
    public $onReceive = null;
    public $onConnect = null;
    public $onClose = null;

    // 连接
    public $socket = null;
    // 创建多个子进程 -》 是不是可以自定义
    protected $config = [
        'worker_num' => 4
    ];
    protected $socket_address = null;
    // 记录子进程pid地址
    protected $workerPidFiles = __DIR__."/workerPids.txt";
    // 以内存的方式存pids
    protected $workerPids = [];

    public function __construct($socket_address)
    {
        $this->socket_address = $socket_address;
    }
    // 需要处理事情
    public function accept()
    {
        Event::add($this->initServer(), $this->createSocket());
    }



    public function initServer()
    {
        // 并不会起到太大的影响
        // 这里是参考与workerman中的写法
        $opts = [
            'socket' => [
                // 设置等待资源的个数
                'backlog' => '102400',
            ],
        ];

        $context = stream_context_create($opts);
        // 设置端口可以重复监听
        \stream_context_set_option($context, 'socket', 'so_reuseport', 1);

        // 传递一个资源的文本 context
        return $this->socket = stream_socket_server($this->socket_address , $errno , $errstr, STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, $context);
    }

    public function createSocket()
    {
        return function($socket){
            // debug(posix_getpid());
            // $client 是不是资源 socket
            $client = stream_socket_accept($this->socket);
            // is_callable判断一个参数是不是闭包
            if (is_callable($this->onConnect)) {
                // 执行函数
                ($this->onConnect)($this, $client);
            }
            // 默认就是循环操作
            Event::add($client, $this->sendClient());
        };
    }

    public function sendClient()
    {
        return function($socket){
            //从连接当中读取客户端的内容
            $buffer=fread($socket,1024);
            //如果数据为空，或者为false,不是资源类型
            if(empty($buffer)){
                if(feof($socket) || !is_resource($socket)){
                    //触发关闭事件
                    swoole_event_del($socket);
                    fclose($socket);
                }
            }
            //正常读取到数据,触发消息接收事件,响应内容
            if(!empty($buffer) && is_callable($this->onReceive)){
                ($this->onReceive)($this, $socket, $buffer);
            }
        };
    }

    public function reloadCli()
    {
        // 先停止运行的进程
        $this->stop();
        pidPut(null, $this->workerPidFiles);
        // 清空记录
        $this->fork();
    }

    public function reload()
    {
        $this->stop();
        $this->fork();

        // $workerPids = $this->workerPids;
        // debug("reloadSig start");
        // debug($workerPids, true);
        // foreach ($workerPids  as $key => $workerPid) {
        //     posix_kill($workerPid, 9);
        //     debug("结束：".$workerPid);
        //     unset($this->workerPids[$key]);
        //     // 删掉一个进程，重启一个进程
        //     $this->fork(1);
        // }
        // debug("reloadSig end");
        // debug($this->workerPids, true);
    }


    public function sigHandler($sig)
    {
        switch ($sig) {
          case SIGUSR1:
            //重启
            $this->reloadSig();
            break;
          case SIGKILL:
            // 停止
            $this->stop();
            break;
        }
    }

    public function stop()
    {
        $workerPids = pidGet($this->workerPidFiles);
        foreach ($workerPids  as $key => $workerPid) {
            posix_kill($workerPid, 9);
        }
    }

    // 启动服务的
    public function start()
    {
        debug('start 开始 访问：'.$this->socket_address);
        pidPut(null, $this->workerPidFiles);
        $this->fork();

        // 答案是后
        $this->monitorWorkersForLinux();
    }

    public function monitorWorkersForLinux()
    {
         // 信号安装
         pcntl_signal(SIGUSR1, [$this, 'sigHandler'], false);
         while (1) {
             // Calls signal handlers for pending signals.
             \pcntl_signal_dispatch();
             // Suspends execution of the current process until a child has exited, or until a signal is delivered
             \pcntl_wait($status);
             // Calls signal handlers for pending signals again.
             \pcntl_signal_dispatch();
         }
    }
    public function fork($workerNum = null)
    {
        $workerNum = (empty($workerNum)) ? $this->config['worker_num'] : $workerNum ;
        for ($i=0; $i < $workerNum; $i++) {
            $son11 = pcntl_fork();
            if ($son11 > 0) {
                // 父进程空间
                pidPut($son11, $this->workerPidFiles);
                $this->workerPids[] = $son11;
            } else if($son11 < 0){
                // 进程创建失败的时候
            } else {
                // debug(posix_getpid()); // 阻塞
                $this->accept();
                // 处理接收请求
                break;
            }
        }
        // for ($i=0; $i < $this->config['worker_num']; $i++) {
        //     $status = 0;
        //     $son = pcntl_wait($status);
        //     debug('回收子进程：'.$son);
        // }
    }
}
?>
```

### 1.6 task任务初体验

#### 1.6.1 task进程

​		Task进程是独立与worker进程的一个进程.他主要处理耗时较长的业务逻辑.并且不影响worker进程处理客户端的请求,这大大提高了swoole的并发能力当有耗时较长的任务时,worker进程通过task()函数把数据投递到Task进程 去处理

##### 1.6.1.1 适用场景

* 情景一：管理员需要给100W用户发送邮件，当点击发送，浏览器会一直转圈，直到邮件全部发送完毕。 
* 情景二：千万微博大V发送一条微博，其关注的粉丝相应的会接收到这个消息，是不是大V需要一直等待消息发送完成，才能执行其它操作 
* 情景三：处理几TB数据 
* 情景四: 数据库插入

​		从我们理解的角度思考，这其实都是php进程一直被阻塞，客户端才一直在等待服务端的响应，我们的代码就是同步执行的。对于用户而言，这就是漫长的等待。如何优雅的提高用户体验就是一个非常棘手的问题

![image-20230201233201987](pic/image-20230201233201987.png)

1. worker进程当中，我们调用对应的task()方法发送数据通知到task worker进程 
2. task worker进程会在onTask()回调中接收到这些数据，并进行处理。
3. 处理完成之后通过调用finsh()函数或者直接return返回消息给worker进程 
4. worker进程在onFinsh()进程收到这些消息并进行处理

#### 1.6.2 task初体验实例

​		这里我们还是推荐直接使用官方的案例代码

```php
// task.php
<?php
    $serv = new Swoole\Server("127.0.0.1", 9501, SWOOLE_BASE);
    $serv->set(array(
        'worker_num' => 2,
        'task_worker_num' => 4,
    ));
    $serv->on('Receive', function(Swoole\Server $serv, $fd, $from_id, $data) {
        echo "接收数据" . $data . "\n";
        $data = trim($data);
        $task_id = $serv->task($data, 0);
        $serv->send($fd, "分发任务，任务id为$task_id\n");
    });
    $serv->on('Task', function (Swoole\Server $serv, $task_id, $from_id, $data) {
        echo "Tasker进程接收到数据";
        echo "#{$serv->worker_id}\tonTask: [PID={$serv->worker_pid}]: task_id=$task_id, data_len=".strlen($data).".".PHP_EOL;
        $serv->finish($data);
    });
    $serv->on('Finish', function (Swoole\Server $serv, $task_id, $data) {
        echo "Task#$task_id finished, data_len=".strlen($data).PHP_EOL;
    });
    $serv->on('workerStart', function($serv, $worker_id) {
        global $argv;
        if($worker_id >= $serv->setting['worker_num']) {
        	swoole_set_process_name("php {$argv[0]}: task_worker");
        } else {
       		swoole_set_process_name("php {$argv[0]}: worker");
        }
    });
    $serv->start();
?>
```

#### 1.6.3 task-ipc-mode的消息队列通信模式

1. 使用unix socket通信，默认模式
2. 使用消息队列通信
3. 使用消息队列通信，并设置为争抢模式

##### 1.6.3.1 Task传递数据的大小问题

​		数据小于8k直接通过管道传递,数据大于8k写入临时文件传递onTask会读取这个文件,把他读出来

![image-20230201233542291](pic/image-20230201233542291.png)

因为没有消费完成会遗留，不过这只是会在意外情况下产生的；注意swoole及时重启了也不会消费；这是在模式1的情况下产生的； 

而模式2：会使用系统的消息队列通信；不过要注意它还是会存在可能的临时文件，重点是重启之后是否会去处理；当然实际上目前的设置之后还是不会处理的，还需要一个额外的参数来指定 

message_queue_key: https://wiki.swoole.com/wiki/page/346.html 

通过这个参数来进行设置，如下为设置方式：

```php
<?php
    $key = ftok(__DIR__, 1);
    $serv->set(array(
        'worker_num' => 2,
        'task_worker_num' => 4,
        'task_ipc_mode' => 2,
        'message_queue_key' => $key
    ));
?>
```

![image-20230201233639971](pic/image-20230201233639971.png)

#### 1.6.4 task问题

​		如果投递的任务量总是大于task进程的处理能力，建议适当的调大task_worker_num的数量，增加task进程数，不然一旦task塞满缓冲区，就会导致worker进程阻塞，所以需要使用好task前期必须有所规划

##### 1.6.4.1 task对worker的影响

​		关于task_worker的个数问题:

![image-20230201233728720](pic/image-20230201233728720.png)

​		除此之外还需要注意的是task可能会影响到worker的工作性能，我们可以看官方的中所提到的

![image-20230201233747267](pic/image-20230201233747267.png)

​		我们可以用代码来做个测试

```php
// swoole_task_worker.php
<?php
    $host = "0.0.0.0:9501\n";
    $serv = new Swoole\Server("0.0.0.0", 9501, SWOOLE_BASE);
    $key = ftok(__DIR__, 1);
    $serv->set(array(
        'worker_num' => 2,
        'task_worker_num' => 4,
        'task_ipc_mode' => 2,
        'message_queue_key' => $key,
        // 处理tcp打包问题
        'open_length_check' => true,
        'package_max_length' => 1024 * 1024 * 3,
        'package_length_type' => 'N',
        'package_length_offset' => 0,
        'package_body_offset' => 4,
    ));
    $serv->on('Receive', function(Swoole\Server $serv, $fd, $from_id, $data) {
        $task_id = $serv->task($data);
        echo "异步事情\n";
        $serv->send($fd, "分发任务，任务id为$task_id\n");
    });
    $serv->on('Task', function (Swoole\Server $serv, $task_id, $from_id, $data) {
    	$serv->finish($data);
    });
    $serv->on('Finish', function (Swoole\Server $serv, $task_id, $data) {
    });
    echo $host;
    $serv->start();
?>
    
// swoole_tcp_client.php
<?php
    $client = new swoole_client(SWOOLE_SOCK_TCP );
    //连接到服务器
    $client->connect('127.0.0.1', 9501, 0.5);
    //向服务器发送数据
    $body = 'a';
    $send = pack('N', strlen($body)) . $body;
    for ($i=0; $i < 100; $i++) {
    	$client->send($send);
    }
    //从服务器接收数据
    $data = $client->recv();
    echo $data."\n";
    //关闭连接
    $client->close();
    echo "其他事情\n";
?>
```

##### 1.6.4.2 其余问题

1. task_max_request 设置task进程的最大任务数。一个task进程在处理完超过此数值的任务后将自动退出。这个参数是为了防止PHP进程内存溢出。如果不希望进程自动退出可以设置为0 
2. 每个woker都有可能投递任务给不同的task_worker处理, 不同的woker进程内存隔离,记录着worker_id, 标识woker进程任务处理数量

#### 1.6.5 task任务切分

​		模拟信息读取短息发送 2： 

​		从200w的MySQL中读取数据，获取到手机号码然后对于用户进行短息发送，通知xxxx事情，然后为了与友好根据性别进行判断‘先生’，‘女士’ 

​		首先第一件事情就是先导入测试的数据与设置MySQL的外部访问的权限-》为了方便导入数据

```tex
[root@localhost mysql] mysql -u root -p
-- 配置远程访问
-- 防火墙端口设置，便于远程访问
[root@localhost mysql] firewall-cmd --zone=public --add-port=3306/tcp --permanent
[root@localhost mysql] firewall-cmd --reload
-- 查看端口
启动防火墙服务：systemctl ummask firewalld 启动防火墙：systemctl start firewalld
-- 进入MySQL
mysql> grant all privileges on *.* to root@'%' identified by "0000";
mysql> flush privileges;
-- 如果没有效果可以尝试重启一下MySQL
```

​		友情提示如下信息为 i3-8 的执行效率

![image-20230201234018893](pic/image-20230201234018893.png)

​		课程中采用的是i7-8

![image-20230201234031336](pic/image-20230201234031336.png)

​		那么如果我们需要实现如上的功能那么对应的sql则是如下

```sql
-- 查询语句
select `name`,gender,mobile from customers;
-- 因考虑数据比较多在实际工作可通过覆盖索引对于该SQL进行优化
alter table customers add index idx_mobile_name_gender(mobile,`name`,gender);
```

![image-20230201234102132](pic/image-20230201234102132.png)

​		SQL优化不在这儿解释

```php
<?php
    require 'db.php';
    $host = "0.0.0.0:9501\n";
    $serv = new Swoole\Server("0.0.0.0", 9501, SWOOLE_BASE);
    $key = ftok(__DIR__, 1);
    $serv->set(array(
        'worker_num' => 2,
        'task_worker_num' => 4,
        'task_ipc_mode' => 2,
        'message_queue_key' => $key,
        // 处理tcp打包问题
        'open_length_check' => true,
        'package_max_length' => 1024 * 1024 * 3,
        'package_length_type' => 'N',
        'package_length_offset' => 0,
        'package_body_offset' => 4,
    ));
    $serv->on('Receive', function(Swoole\Server $serv, $fd, $from_id, $data) use ($db){
        for ($i = 0; $i < 4; $i++) {
            $data = $db->query('select id,gender,name,mobile from customers where id > '.($i * 500000).' and id <= '.(($i + 1) * 500000));
            $task_id = $serv->task($data, $i);
            unset($data);
        }
        $serv->send($fd, "分发任务\n");
    });
    $serv->on('Task', function (Swoole\Server $serv, $task_id, $from_id, $data) {
        echo "处理: ".$from_id." 的任务，任务id为".$task_id."数据信息量".count($data)."\n";
        echo "当前处理进程:".posix_getpid()."\n";
        // 就当做发送短信
        file_put_contents(__DIR__.'/task_'.$task_id.'.log', '');
        foreach ($data as $key => $value) {
            try {
                if ($value['gender'] == 0) {
               	 // 女士
                	$string = "尊敬的手机号为".$value['mobile']."的".$value['name']."_女士_你好，然后xxxxxx省....\n";
                } else {
                	// 先生
                	$string = "尊敬的手机号为".$value['mobile']."的".$value['name']."_先生_你好，然后xxxxxx省....\n";
                }
            	// 不打印就记录 -- 因为信息太多
				file_put_contents(__DIR__.'/task_'.$task_id.'.log', $string, 8);
            } catch (\Exception $e) {
            	// $serv->sendMessage($value, 1);
            }
        }
        $serv->finish($data);
    });
    $serv->on('Finish', function (Swoole\Server $serv, $task_id, $data) { });
    $serv->on('PipeMessage', function (Swoole\Server $server, $src_worker_id, $message){
        echo "来自".$src_worker_id."的信息\n";
        var_dump($message);
    });
    echo $host;
    $serv->start();
?>
```

![image-20230201234307715](https://gitee.com/jinjiedasheng/swoole-shouce/raw/master/swoole_pic/image-20230201234307715.png)

